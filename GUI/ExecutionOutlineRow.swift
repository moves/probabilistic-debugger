import ASTExecution
import SimpleLanguageAST

fileprivate extension ASTExecutionState {
  func sourceCode(_ sourceCode: String) -> String {
    if let nextStmt = remainingStmts.first {
      return String(sourceCode.split(separator: "\n")[nextStmt.range.lowerBound.line - 1]).trimmingCharacters(in: .whitespaces)
    } else {
      return "end"
    }
  }
}

enum ExecutionOutlineRow: Identifiable, Hashable {
  /// A leaf entry with no children that represents execution of a statement
  /// - `name` is the source code of the statement that is about to be executed
  /// - `state` is the execution state before the statement represented by this row was executed
  /// - `ast` is the AST whose execution `state` models
  /// - `id` uniquely identifies this `ExecutionOutlineRow`
  case entry(name: String, state: ASTExecutionState, ast: [Stmt], id: Int)

  /// A intermediate row that has child rows.
  /// - `name` is a description of this row. Either source code or something like `true-Branch`.
  /// - If the row represents the execution of a statement, `state` is the execution state before the statement represented by this row was executed, otherwise `nil`.
  /// - `children` are the sub-rows of this row.
  /// - `ast` is the AST whose execution `state` models
  /// - `id` uniquely identifies this `ExecutionOutlineRow`
  case sequence(name: String, state: ASTExecutionState?, children: [ExecutionOutlineRow], ast: [Stmt], id: Int)

  private static var nextUnusedId = 1

  private static func getNextId() -> Int {
    defer {
      nextUnusedId += 1
    }
    return nextUnusedId
  }

  // MARK: - Equatable, Hashable, Identifiable conformance

  static func == (lhs: ExecutionOutlineRow, rhs: ExecutionOutlineRow) -> Bool {
    return lhs.id == rhs.id
  }

  func hash(into hasher: inout Hasher) {
    id.hash(into: &hasher)
  }

  var id: Int {
    switch self {
    case .entry(name: _, state: _, ast: _, id: let id):
      return id
    case .sequence(name: _, state: _, children: _, ast: _, id: let id):
      return id
    }
  }

  // MARK: - Retrieving common members

  var state: ASTExecutionState? {
    switch self {
    case .entry(name: _, state: let state, ast: _, id: _):
      return state
    case .sequence(name: _, state: let state, children: let children, ast: _, id: _):
      return state ?? children.first?.state
    }
  }

  var name: String {
    switch self {
    case .entry(name: let name, state: _, ast: _, id: _):
      return name
    case .sequence(name: let name, state: _, children: _, ast: _, id: _):
      return name
    }
  }

  var isSourceCode: Bool {
    switch self {
    case .entry:
      return true
    case .sequence(name: _, state: let state, children: _, ast: _, id: _):
      return state != nil
    }
  }

  var children: [ExecutionOutlineRow]? {
    switch self {
    case .entry:
      return nil
    case .sequence(name: _, state: _, children: let children, ast: _, id: _):
      return children
    }
  }

  var ast: [Stmt] {
    switch self {
    case .entry(name: _, state: _, ast: let ast, id: _):
      return ast
    case .sequence(name: _, state: _, children: _, ast: let ast, id: _):
      return ast
    }
  }

  // MARK: - Construction from ExecutionOutline

  static func fromExecutionOutline(_ executionOutline: ExecutionOutline, sourceCode: String, ast: [Stmt]) -> [ExecutionOutlineRow] {
    return executionOutline.entries.map {
      Self.fromExecutionOutlineEntry($0, sourceCode: sourceCode, ast: ast)
    }
  }

  static func fromExecutionOutlineEntry(_ executionOutlineEntry: ExecutionOutlineEntry, sourceCode: String, ast: [Stmt]) -> ExecutionOutlineRow {
    switch executionOutlineEntry {
    case .statement(let state):
      return .entry(
        name: state.sourceCode(sourceCode),
        state: executionOutlineEntry.state,
        ast: ast,
        id: getNextId()
      )
    case .end:
      return .entry(
        name: "end",
        state: executionOutlineEntry.state,
        ast: ast,
        id: getNextId()
      )
    case .branch(state: let state, true: let trueBranch, false: let falseBranch):
      let trueEntry = trueBranch.map {
        ExecutionOutlineRow.sequence(
          name: "true-branch",
          state: nil,
          children: fromExecutionOutline($0, sourceCode: sourceCode, ast: ast),
          ast: ast,
          id: getNextId()
        )
      }
      let falseEntry = falseBranch.map {
        ExecutionOutlineRow.sequence(
          name: "false-branch",
          state: nil,
          children: fromExecutionOutline($0, sourceCode: sourceCode, ast: ast),
          ast: ast,
          id: getNextId()
        )
      }
      return .sequence(
        name: state.sourceCode(sourceCode),
        state: state,
        children: [trueEntry, falseEntry].compactMap { $0 },
        ast: ast,
        id: getNextId()
      )
    case .loop(state: let state, iterations: let iterations, exitStates: let exitStates):
      let iterationRows = iterations.enumerated().map { (index, iterationOutline) in
        ExecutionOutlineRow.sequence(
          name: "Iteration \(index + 1)",
          state: nil,
          children: fromExecutionOutline(iterationOutline, sourceCode: sourceCode, ast: ast),
          ast: ast,
          id: getNextId()
        )
      }
      let iterationRowsSequence = ExecutionOutlineRow.sequence(
        name: "Iterations",
        state: nil,
        children: iterationRows,
        ast: ast,
        id: getNextId()
      )

      let exitStateRows = exitStates.enumerated().map { (index, exitState) in
        ExecutionOutlineRow.entry(
          name: "≤ \(index) iterations",
          state: exitState,
          ast: ast,
          id: getNextId()
        )
      }
      let exitStateRowsSequence = ExecutionOutlineRow.sequence(
        name: "Exit States",
        state: nil,
        children: exitStateRows,
        ast: ast,
        id: getNextId()
      )
      
      return .sequence(
        name: state.sourceCode(sourceCode),
        state: state,
        children: [iterationRowsSequence, exitStateRowsSequence],
        ast: ast,
        id: getNextId()
      )
    }
  }
}
