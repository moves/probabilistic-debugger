import ASTExecution
import SimpleLanguageAST
import Foundation

fileprivate extension VariableValue {
  var pseudoIntValue: Int {
    switch self {
    case .integer(let value):
      return value
    case .bool(let value):
      return value ? 1 : 0
    }
  }
}

struct VariableDistributionRow: Identifiable, Hashable {
  var id: String {
    return variable.description
  }

  let variable: SourceVariable
  let distribution: [VariableValue: ClosedRange<Double>]
  var average: ClosedRange<Double> {
    let lowerBound = distribution.map({ (varValue, probability) in Double(varValue.pseudoIntValue) * probability.lowerBound }).reduce(0, +)
    let upperBound = distribution.map({ (varValue, probability) in Double(varValue.pseudoIntValue) * probability.upperBound }).reduce(0, +)
    return lowerBound...upperBound
  }
  var valuesDescription: String {
    return distribution.map { (varValue, probability) in
      "\(varValue.description): \(probability.percentageDescription(decimalPlaces: 2))"
    }
    .sorted {
      return NSString(string: $0).compare($1, options: .numeric) == ComparisonResult.orderedAscending
    }
    .joined(separator: "\n")
  }
}
