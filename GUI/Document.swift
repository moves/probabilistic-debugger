import SwiftUI
import UniformTypeIdentifiers

class SourceCodeDocument: FileDocument {
  static var readableContentTypes = [UTType(importedAs: "com.example.plain-text")]
  
  required init(configuration: ReadConfiguration) throws {
    guard let data = configuration.file.regularFileContents,
          let string = String(data: data, encoding: .utf8) else {
            throw CocoaError(.fileReadCorruptFile)
          }
    sourceCode = string
  }
  
  func fileWrapper(configuration: WriteConfiguration) throws -> FileWrapper {
    let data = sourceCode.data(using: .utf8)!
    return .init(regularFileWithContents: data)
  }
  
  var sourceCode = ""

  init() {
  }
//
//  override class var autosavesInPlace: Bool {
//    return true
//  }
//
//  override func makeWindowControllers() {
//    // Create the SwiftUI view that provides the window contents.
//    let contentView = ContentView(model: Model(sourceCode: sourceCode))
//
//    // Create the window and set the content view.
//    let window = NSWindow(
//      contentRect: NSRect(x: 0, y: 0, width: 1000, height: 600),
//      styleMask: [.titled, .closable, .miniaturizable, .resizable, .fullSizeContentView],
//      backing: .buffered, defer: false)
//    window.isReleasedWhenClosed = false
//    window.center()
//    window.contentView = NSHostingView(rootView: contentView)
//    let windowController = NSWindowController(window: window)
//    self.addWindowController(windowController)
//  }
//
//  override func data(ofType typeName: String) throws -> Data {
//    return self.sourceCode.data(using: .utf8)!
//  }
//
//  override func read(from data: Data, ofType typeName: String) throws {
//    self.sourceCode = String(data: data, encoding: .utf8)!
//  }
}

