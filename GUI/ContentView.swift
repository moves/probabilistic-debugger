import Combine
import SwiftUI
import ASTWPInference
import ASTExecution
import SimpleLanguageParser
import SimpleLanguageTypeChecker
import SimpleLanguageAST

fileprivate extension Double {
  func rounded(decimalPlaces: Int) -> Double {
    let padding = pow(10, Double(decimalPlaces))
    return (self * padding).rounded() / padding
  }
}

struct ContentView: View {
  @ObservedObject var model: Model

  var body: some View {
    HSplitView {
      if case .failure(let error) = model.astAndSource {
        VStack {
        Text("Compliation error")
            .font(Font.system(Font.TextStyle.title))
        Text(String(describing: error))
        }
      } else {
        List(model.outline, id: \.self, children: \.children, selection: $model.selectedOutlineRow) { entry in
          VStack(alignment: .leading) {
            if entry.isSourceCode {
              Text(entry.name)
                .font(.custom("Menlo", fixedSize: 12))
            } else {
              Text(entry.name)
                .font(.system(size: 12).italic())
            }
          }
        }
      }
      
      VSplitView {
        VStack(alignment: .leading) {
          ForEach(model.sourceCodeLines) { (line) in
            if line.isSelected {
              Text(line.text)
                .font(.custom("Menlo", fixedSize: 12))
                .frame(maxWidth: .infinity, alignment: .leading)
                .background(Color(#colorLiteral(red: 0.8431372549, green: 0.9098039216, blue: 0.8549019608, alpha: 1)))
            } else {
              Text(line.text)
                .font(.custom("Menlo", fixedSize: 12))
            }
          }
          .frame(maxWidth: .infinity, alignment: .leading)
          .background(Color.white)

          Spacer()
        }
        .background(Color.white)
      
        VStack(alignment: .leading, spacing: 2) {
        HStack {
          Button("⤼") {
            model.executeCommand(.stepOver)
          }
          .padding(.leading, 5)
          .font(.system(size: 20))
          .padding(.trailing, 5)

          Button("↓✓") {
            model.executeCommand(.stepInto(branch: true))
          }
          .padding(.trailing, 5)

          Button("↓✗") {
            model.executeCommand(.stepInto(branch: false))
          }

          Spacer()

          Text("Samples: \((model.variableDistributions.reachabilityProbability * 100).rounded(decimalPlaces: 4).description)%")
            .padding(.trailing, 10)
        }
        .buttonStyle(BorderlessButtonStyle())
          
        List(Array(model.variableDistributions.rows.enumerated()), id: \.1) { (index, row) in
          VStack(alignment: .leading) {
            if index == 0 {
              HStack {
                Text("Variable")
                  .frame(width: 100, alignment: .leading)
                Text("Average")
                  .frame(width: 90, alignment: .leading)
                Text("Values")
                  .frame(width: 200, alignment: .leading)
              }
              .font(Font.system(size: 13).bold())
              Divider()
            }
            HStack {
              Text(row.variable.description)
                .frame(width: 100, alignment: .leading)
              Text(row.average.description(decimalPlaces: 2))
                .frame(width: 90, alignment: .leading)
              Text(row.valuesDescription)
                .fixedSize()
                .frame(width: 200, alignment: .leading)
            }
            Divider()
          }
        }
        }
      }
    }
  }
}
