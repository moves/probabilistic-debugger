import ASTExecution
import ASTWPInference
import Combine
import Dispatch
import SimpleLanguageAST
import SimpleLanguageParser
import SimpleLanguageTypeChecker
import Foundation

/// A single line in the source code to be displayed in the UI.
struct SourceCodeLine: Identifiable {
  /// The text of the line
  let text: String

  /// The line number
  let lineNo: Int

  /// Whether the line is the next to be executed in the debugger.
  let isSelected: Bool

  var id: Int { lineNo }
}

extension ClosedRange where Bound == Double {
  func percentageDescription(decimalPlaces: Int) -> String {
    let lowerRounded = (lowerBound * 100).rounded(decimalPlaces: decimalPlaces)
    var upperRounded = (upperBound * 100).rounded(decimalPlaces: decimalPlaces)
    if upperRounded > 100 {
      upperRounded = 100
    }
    let averageRounded = ((lowerRounded + upperRounded) / 2).rounded(decimalPlaces: decimalPlaces)
    let errorRouned = ((upperRounded - lowerRounded) / 2).rounded(decimalPlaces: decimalPlaces)
    if errorRouned == 0 {
      return "\(averageRounded.description)%"
    } else {
      return "\(averageRounded.description)% (± \(errorRouned.description)%)"
    }
  }
  
  func description(decimalPlaces: Int) -> String {
    let lowerRounded = lowerBound.rounded(decimalPlaces: decimalPlaces)
    let upperRounded = upperBound.rounded(decimalPlaces: decimalPlaces)
    let averageRounded = ((lowerRounded + upperRounded) / 2).rounded(decimalPlaces: decimalPlaces)
    let errorRouned = ((upperRounded - lowerRounded) / 2).rounded(decimalPlaces: decimalPlaces)
    if errorRouned == 0 {
      return "\(averageRounded.description)"
    } else {
      return "\(averageRounded.description) (± \(errorRouned.description))"
    }
  }
}

struct VariableDistributionResult {
  /// The variables defined at the current debugger position and their values
  let rows: [VariableDistributionRow]

  /// The probability of reaching the current state with the current execution history.
  let reachabilityProbability: Double
}

/// Represents all state of the current debugger instance.
class Model: ObservableObject {
  /// The source code that is being debugged.
  @Published var sourceCode: String

  /// After successful compilation, the AST of the debugged source code and the source code, otherwise the compilation error.
  /// We need to store the source code in this variable as well to make sure we always have a matching source code - AST pair and don't run into a situation where the source code has changed but the AST hasn't been updated to reflect the change yet.
  @Published var astAndSource: Result<(ast: [Stmt], source: String), Error> = .success(([], ""))

  /// The execution outline of the debugged AST.
  @Published var outline: [ExecutionOutlineRow] = []

  /// The currently selected execution outline row in the debugger.
  /// Will be set to the outline’s start on recompilation and is set to `nil` if the user invokes a manual debugger command.
  @Published var selectedOutlineRow: ExecutionOutlineRow?

  /// The current execution state and the AST that is being debugged.
  /// We need to store the AST here to make sure we don’t run into a situation where the AST has been updated but the state hasn’t changed to reflect the change yet.
  @Published var currentStateAndAst: (state: ASTExecutionState, ast: [Stmt])?

  /// The source code split into lines with the line of the current deubugger state highlighted.
  @Published var sourceCodeLines: [SourceCodeLine] = []

  /// The variable distributions at the current debuggers state, refined using WP.
  @Published var variableDistributions = VariableDistributionResult(rows: [], reachabilityProbability: 0)

  init(sourceCode: String) {
    self.sourceCode = sourceCode

    // When the source code changes, recompile and save the AST.
    $sourceCode
      .receive(on: DispatchQueue.global(qos: .userInitiated))
      .map { (source) -> Result<(ast: [Stmt], source: String), Error> in
        do {
          let parser = Parser(sourceCode: source)
          let parsedAst = try parser.parseFile()
          return .success((try TypeCheckPipeline.typeCheck(stmts: parsedAst), source))
        } catch {
          return .failure(error)
        }
      }
      .receive(on: DispatchQueue.main)
      .assign(to: &$astAndSource)

    // When the AST changes, recompute the outline.
    $astAndSource
      .receive(on: DispatchQueue.global(qos: .userInitiated))
      .map { astAndSourceResult -> [ExecutionOutlineRow] in
        switch astAndSourceResult {
        case .success((let ast, let source)):
          let executionOutline = ExecutionOutlineGenerator.generateExecutionOutline(stmts: ast, numSamples: 10_000)
          return ExecutionOutlineRow.fromExecutionOutline(executionOutline, sourceCode: source, ast: ast)
        case .failure:
          return []
        }
      }
      .receive(on: DispatchQueue.main)
      .assign(to: &$outline)

    // When the outline changes, set the current state to the outline's beginning.
    $outline
      .map { (outline) -> ExecutionOutlineRow? in
        return outline.first
      }
      .assign(to: &$selectedOutlineRow)

    // When the outline row selection changes, change the current execution state.
    $selectedOutlineRow
      .map { (selectedRow) -> (ASTExecutionState, [Stmt])? in
        if let selectedRow = selectedRow, let state = selectedRow.state {
          return (state, selectedRow.ast)
        } else {
          return nil
        }
      }
      .assign(to: &$currentStateAndAst)

    // When the current outline state changes, recompute the variable probabilities.
    $currentStateAndAst
      .receive(on: DispatchQueue.global(qos: .userInitiated))
      .map { (currentStateAndAst) -> VariableDistributionResult in
        guard let (currentState, ast) = currentStateAndAst else {
          return VariableDistributionResult(rows: [], reachabilityProbability: 0)
        }
        let variableDistributionRows = currentState.variableValuesRefinedUsingWp(ast: ast)
          .map { (variable, distribution) in
            VariableDistributionRow(variable: variable, distribution: distribution)
          }
          .sorted {
            return NSString(string: $0.variable.description).compare($1.variable.description, options: .numeric) == ComparisonResult.orderedAscending
          }
        return VariableDistributionResult(
          rows: variableDistributionRows,
          reachabilityProbability: currentState.reachabilityProbability(ast: ast)
        )
      }
      .receive(on: DispatchQueue.main)
      .assign(to: &$variableDistributions)

    // When the source code or the current state changes, recompute the source lines with the current line highlighted.
    Publishers.CombineLatest($sourceCode, $currentStateAndAst)
      .map { (sourceCode, currentStateAndAst) -> [SourceCodeLine] in
        let lines = sourceCode.split(separator: "\n").map(String.init) + [" "]
        let selectedLine: Int?
        if let currentStateAndAst = currentStateAndAst {
          if let nextStmt = currentStateAndAst.0.remainingStmts.first {
            selectedLine = nextStmt.range.lowerBound.line - 1
          } else {
            selectedLine = lines.count - 1
          }
        } else {
          selectedLine = nil
        }
        return lines.enumerated().map { (lineNo, text) -> SourceCodeLine in
          return SourceCodeLine(text: text, lineNo: lineNo, isSelected: lineNo == selectedLine)
        }
      }
      .assign(to: &$sourceCodeLines)
  }

  /// Update the current state by executing the given debugger command.
  func executeCommand(_ command: ExecutionCommand) {
    guard let (currentState, ast) = currentStateAndAst else {
      return
    }
    guard !currentState.remainingStmts.isEmpty else {
      return
    }
    selectedOutlineRow = nil
    currentStateAndAst = (currentState.executeOneCommand(command), ast)
  }
}
