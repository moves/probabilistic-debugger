import SwiftUI

@main
struct afasfsadfApp: App {
    var body: some Scene {
        DocumentGroup(newDocument: SourceCodeDocument()) { file in
          ContentView(model: Model(sourceCode: file.document.sourceCode))
        }
    }
}
