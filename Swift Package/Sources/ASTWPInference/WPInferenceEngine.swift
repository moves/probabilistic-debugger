import ASTExecution
import SimpleLanguageAST

public enum WPInferenceEngine {
  // MARK: Inference with execution histories
  
  /// Perform WP-inference for the given execution history, using `term` as the initial term for the computation of `wp(f)`.
  public static func infer(
    term: WPTerm,
    for executionHistory: AnnotatedExecutionHistory,
    loopBounds: LoopUnrolls
  ) -> WPInferenceTerm {
    let initialTerm = WPInferenceTerm(
      wpF: term,
      wlpOne: .integer(1),
      wlpZero: .integer(0),
      woipOne: .integer(1)
    )
    return infer(inferenceTerm: initialTerm, for: executionHistory, loopBounds: loopBounds)
  }
  
  /// Perform WP-inference for the given `executionHistory`, using `inferenceTerm` as the starting term.
  private static func infer(
    inferenceTerm: WPInferenceTerm,
    for executionHistory: AnnotatedExecutionHistory,
    loopBounds: LoopUnrolls
  ) -> WPInferenceTerm {
    var inferenceTerm = inferenceTerm
    
    for (command, stmt) in executionHistory.entries.reversed() {
      switch stmt.asEnum {
      case .variableDecl, .assign, .observe:
        /// Inference of these straight-line statements is independent of the chosen debugger command.
        inferenceTerm = infer(
          inferenceTerm: inferenceTerm,
          stmts: [stmt],
          loopBounds: loopBounds
        )
      case .codeBlock:
        fatalError("Annotated execution histories shouldn't contain CodeBlockStmts")
      case .ifStmt(let ifStmt):
        inferenceTerm = inferBranchingStmt(
          inferenceTerm: inferenceTerm,
          command: command,
          stmt: ifStmt,
          condition: ifStmt.condition,
          loopBounds: loopBounds
        )
      case .ifElse(let ifElseStmt):
        inferenceTerm = inferBranchingStmt(
          inferenceTerm: inferenceTerm,
          command: command,
          stmt: ifElseStmt,
          condition: ifElseStmt.condition,
          loopBounds: loopBounds
        )
      case .prob(let probStmt):
        inferenceTerm = inferProbStmt(
          inferenceTerm: inferenceTerm,
          stmt: probStmt,
          probability: probStmt.probability,
          command: command,
          loopBounds: loopBounds
        )
      case .whileStmt(let whileStmt):
        inferenceTerm = inferBranchingStmt(
          inferenceTerm: inferenceTerm,
          command: command,
          stmt: whileStmt,
          condition: whileStmt.condition,
          loopBounds: loopBounds
        )
      }
    }
    return inferenceTerm
  }
  
  /// Perform WP-inference of a branching statement (i.e. `if`, `if-else` or `while`-stmt).
  private static func inferBranchingStmt(
    inferenceTerm: WPInferenceTerm,
    command: ExecutionCommand,
    stmt: Stmt,
    condition: Expr,
    loopBounds: LoopUnrolls
  ) -> WPInferenceTerm {
    switch command {
    case .stepOver:
      return infer(inferenceTerm: inferenceTerm, stmts: [stmt], loopBounds: loopBounds)
    case .stepInto(branch: let branch):
      return WPInferenceTerm(
        wpF: .boolToInt(.equal(lhs: .fromExpression(condition), rhs: .bool(branch))) * inferenceTerm.wpF,
        wlpOne: .boolToInt(.equal(lhs: .fromExpression(condition), rhs: .bool(branch))) * inferenceTerm.wlpOne,
        wlpZero: .boolToInt(.equal(lhs: .fromExpression(condition), rhs: .bool(branch))) * inferenceTerm.wlpZero,
        woipOne: inferenceTerm.woipOne
      )
    }
  }
  
  private static func inferProbStmt(
    inferenceTerm: WPInferenceTerm,
    stmt: Stmt,
    probability: Double,
    command: ExecutionCommand,
    loopBounds: LoopUnrolls
  ) -> WPInferenceTerm {
    switch command {
    case .stepOver:
      return infer(inferenceTerm: inferenceTerm, stmts: [stmt], loopBounds: loopBounds)
    case .stepInto(branch: true):
      return WPInferenceTerm(
        wpF: WPTerm.double(probability) * inferenceTerm.wpF,
        wlpOne: WPTerm.double(probability) * inferenceTerm.wlpOne,
        wlpZero: WPTerm.double(probability) * inferenceTerm.wlpZero,
        woipOne: inferenceTerm.woipOne
      )
    case .stepInto(branch: false):
      return WPInferenceTerm(
        wpF: WPTerm.double(1 - probability) * inferenceTerm.wpF,
        wlpOne: WPTerm.double(1 - probability) * inferenceTerm.wlpOne,
        wlpZero: WPTerm.double(1 - probability) * inferenceTerm.wlpZero,
        woipOne: inferenceTerm.woipOne
      )
    }
  }
  
  // MARK: Inference without execution histories
  
  /// Perform WP-inference of the given `inferenceTerm` for the given `stmts`.
  /// This does not take an execution history into account.
  private static func infer(inferenceTerm: WPInferenceTerm, stmts: [Stmt], loopBounds: LoopUnrolls) -> WPInferenceTerm {
    var inferenceTerm = inferenceTerm
    for stmt in stmts.reversed() {
      switch stmt.asEnum {
      case .variableDecl(let varDecl):
        inferenceTerm = inferVariableAssignment(
          inferenceTerm: inferenceTerm,
          variable: varDecl.variable,
          assignment: varDecl.expr
        )
        
      case .assign(let varAssign):
        guard case .resolved(let variable) = varAssign.variable else {
          fatalError("Variable not resolved during type checking?")
        }
        inferenceTerm = inferVariableAssignment(
          inferenceTerm: inferenceTerm,
          variable: variable,
          assignment: varAssign.expr
        )
      case .observe(let observe):
        inferenceTerm = WPInferenceTerm(
          wpF: .boolToInt(.fromExpression(observe.condition)) * inferenceTerm.wpF,
          wlpOne: .boolToInt(.fromExpression(observe.condition)) * inferenceTerm.wlpOne,
          wlpZero: .boolToInt(.fromExpression(observe.condition)) * inferenceTerm.wlpZero,
          woipOne: inferenceTerm.woipOne
        )
      case .codeBlock:
        fatalError("Annotated execution histories shouldn't contain CodeBlockStmts")
      case .ifStmt(let ifStmt):
        let ifTerm = infer(inferenceTerm: inferenceTerm, stmts: ifStmt.body.body, loopBounds: loopBounds)
        let condTerm = WPTerm.boolToInt(.fromExpression(ifStmt.condition))
        let notCondTerm = WPTerm.boolToInt(.not(.fromExpression(ifStmt.condition)))
        inferenceTerm = condTerm * ifTerm + notCondTerm * inferenceTerm
      case .ifElse(let ifElseStmt):
        let ifTerm = infer(inferenceTerm: inferenceTerm, stmts: ifElseStmt.ifBody.body, loopBounds: loopBounds)
        let elseTerm = infer(inferenceTerm: inferenceTerm, stmts: ifElseStmt.elseBody.body, loopBounds: loopBounds)
        let condTerm = WPTerm.boolToInt(.fromExpression(ifElseStmt.condition))
        let notCondTerm = WPTerm.boolToInt(.not(.fromExpression(ifElseStmt.condition)))
        inferenceTerm = condTerm * ifTerm + notCondTerm * elseTerm
      case .prob(let probStmt):
        let ifTerm = infer(inferenceTerm: inferenceTerm, stmts: probStmt.ifBody.body, loopBounds: loopBounds)
        let elseTerm = infer(inferenceTerm: inferenceTerm, stmts: probStmt.elseBody?.body ?? [], loopBounds: loopBounds)
        inferenceTerm = .double(probStmt.probability) * ifTerm + .double(1 - probStmt.probability) * elseTerm
      case .whileStmt(let whileStmt):
        guard let loopBound = loopBounds.values[whileStmt.nodeId] else {
          fatalError("When performing WP-inference all loops should have assocated loop bounds.")
        }
        let condTerm = WPTerm.boolToInt(.fromExpression(whileStmt.condition))
        let notCondTerm = WPTerm.boolToInt(.not(.fromExpression(whileStmt.condition)))
        inferenceTerm = WPInferenceTerm(
          wpF: notCondTerm * inferenceTerm.wpF,
          wlpOne: condTerm + notCondTerm * inferenceTerm.wlpOne,
          wlpZero: condTerm + notCondTerm * inferenceTerm.wlpZero,
          woipOne: notCondTerm * inferenceTerm.woipOne
        )
        for _ in 0..<loopBound {
          inferenceTerm = condTerm * infer(inferenceTerm: inferenceTerm, stmts: whileStmt.body.body, loopBounds: loopBounds) +
            notCondTerm * inferenceTerm
        }
      }
    }
    return inferenceTerm
  }
  
  private static func inferVariableAssignment(inferenceTerm: WPInferenceTerm, variable: SourceVariable, assignment expr: Expr) -> WPInferenceTerm {
    return WPInferenceTerm(
      wpF: inferenceTerm.wpF.replacing(variable: variable, with: .fromExpression(expr)) ?? inferenceTerm.wpF,
      wlpOne: inferenceTerm.wlpOne.replacing(variable: variable, with: .fromExpression(expr)) ?? inferenceTerm.wlpOne,
      wlpZero: inferenceTerm.wlpZero.replacing(variable: variable, with: .fromExpression(expr)) ?? inferenceTerm.wlpZero,
      woipOne: inferenceTerm.woipOne.replacing(variable: variable, with: .fromExpression(expr)) ?? inferenceTerm.woipOne
    )
  }
}
