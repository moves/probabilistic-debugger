import ASTExecution
import SimpleLanguageAST

fileprivate extension WPTerm {
  static func probability(of variable: SourceVariable, equalTo variableValue: VariableValue) -> WPTerm {
    switch variableValue {
    case .integer(let value):
      return .probability(of: variable, equalTo: WPTerm.integer(value))
    case .bool(let value):
      return .probability(of: variable, equalTo: WPTerm.bool(value))
    }
  }
}

public extension ASTExecutionState {
  private func annotatedExecutionHistory(ast: [Stmt]) -> AnnotatedExecutionHistory {
    return AnnotatedExecutionHistory(from: executionHistory, stmts: ast)
  }

  func variableValuesRefinedUsingWp(ast: [Stmt]) -> [SourceVariable: [VariableValue: ClosedRange<Double>]] {
    var variableValues: [SourceVariable: [VariableValue: ClosedRange<Double>]] = [:]
    for variable in liveVariables {
      let possibleValues = Set(samples.map({ $0.values[variable]! }))
      var variableDistribution: [VariableValue: ClosedRange<Double>] = [:]
      for value in possibleValues {
        variableDistribution[value] = WPInferenceEngine.infer(
          term: .probability(of: variable, equalTo: value),
          for: annotatedExecutionHistory(ast: ast),
          loopBounds: finishedLoopUnrolls
        ).bounds
      }
      variableValues[variable] = variableDistribution
    }
    return variableValues
  }
  
  func reachabilityProbability(ast: [Stmt]) -> Double {
    return WPInferenceEngine.infer(
      term: .integer(1),
      for: annotatedExecutionHistory(ast: ast),
      loopBounds: finishedLoopUnrolls
    ).wpF.doubleValue
  }
}
