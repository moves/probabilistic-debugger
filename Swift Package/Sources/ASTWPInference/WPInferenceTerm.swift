/// A triple that contains the computation results of `wp(f)`, `wp(1)` and `woip(1)`
/// or `wph(f)`, `wph(1)` and `woiph(1)` if the term is computed using an execution history.
public struct WPInferenceTerm: Equatable {
  public let wpF: WPTerm
  public let wlpOne: WPTerm
  public let wlpZero: WPTerm
  public let woipOne: WPTerm
  
  public var lowerBound: Double {
    return (wpF / wlpOne).doubleValue
  }
  
  public var upperBound: Double {
    return ((wpF + .integer(1) - woipOne) / (wlpOne - wlpZero)).doubleValue
  }
  
  public var bounds: ClosedRange<Double> {
    if upperBound < lowerBound {
      // Due to numerical instabilities, we might end up with
      // upperBound < lowerBound. Hotfix the issue by increasing the upper bound
      return lowerBound...lowerBound
    }
    return lowerBound...upperBound
  }
}

public func *(lhs: WPTerm, rhs: WPInferenceTerm) -> WPInferenceTerm {
  return WPInferenceTerm(
    wpF: lhs * rhs.wpF,
    wlpOne: lhs * rhs.wlpOne,
    wlpZero: lhs * rhs.wlpZero,
    woipOne: lhs * rhs.woipOne
  )
}

public func +(lhs: WPInferenceTerm, rhs: WPInferenceTerm) -> WPInferenceTerm {
  return WPInferenceTerm(
    wpF: lhs.wpF + rhs.wpF,
    wlpOne: lhs.wlpOne + rhs.wlpOne,
    wlpZero: lhs.wlpZero + rhs.wlpZero,
    woipOne: lhs.woipOne + rhs.woipOne
  )
}
