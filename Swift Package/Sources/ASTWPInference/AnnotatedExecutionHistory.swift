import ASTExecution
import SimpleLanguageAST

/// A list of execution commands where each command is augumented by the statment it was executed on.
public struct AnnotatedExecutionHistory {
  public let entries: [(ExecutionCommand, Stmt)]
  
  /// Form an `AnnotatedExecutionHistory` based on a list of execution commands and a list of statements the commands have been executed on.
  /// In the `AnnotatedExecutionHistory`, all `CodeBlockStmt`s are expanced and no command is executed on a `CodeBlockStmt`.
  public init(from executionHistory: [ExecutionCommand], stmts: [Stmt]) {
    var remainingStmts = stmts
    var annotatedEntries: [(ExecutionCommand, Stmt)] = []
    annotatedEntries.reserveCapacity(executionHistory.count)
    
    for command in executionHistory {
      guard let nextStmt = remainingStmts.first else {
        fatalError("Executing past program end?")
      }
      switch nextStmt.asEnum {
      case .variableDecl, .assign, .observe:
        annotatedEntries.append((command, nextStmt))
        remainingStmts = Array(remainingStmts.dropFirst())
      case .codeBlock(let codeBlock):
        remainingStmts = codeBlock.body + Array(remainingStmts.dropFirst())
      case .ifStmt(let ifStmt):
        switch command {
        case .stepOver:
          annotatedEntries.append((command, nextStmt))
          remainingStmts = Array(remainingStmts.dropFirst())
        case .stepInto(branch: true):
          annotatedEntries.append((command, nextStmt))
          remainingStmts = ifStmt.body.body + Array(remainingStmts.dropFirst())
        case .stepInto(branch: false):
          annotatedEntries.append((command, nextStmt))
          remainingStmts = Array(remainingStmts.dropFirst())
        }
      case .ifElse(let ifElseStmt):
        switch command {
        case .stepOver:
          annotatedEntries.append((command, nextStmt))
          remainingStmts = Array(remainingStmts.dropFirst())
        case .stepInto(branch: true):
          annotatedEntries.append((command, nextStmt))
          remainingStmts = ifElseStmt.ifBody.body + Array(remainingStmts.dropFirst())
        case .stepInto(branch: false):
          annotatedEntries.append((command, nextStmt))
          remainingStmts = ifElseStmt.elseBody.body + Array(remainingStmts.dropFirst())
        }
      case .prob(let probStmt):
        switch command {
        case .stepOver:
          annotatedEntries.append((command, nextStmt))
          remainingStmts = Array(remainingStmts.dropFirst())
        case .stepInto(branch: true):
          annotatedEntries.append((command, nextStmt))
          remainingStmts = probStmt.ifBody.body + Array(remainingStmts.dropFirst())
        case .stepInto(branch: false):
          annotatedEntries.append((command, nextStmt))
          remainingStmts = (probStmt.elseBody?.body ?? []) + Array(remainingStmts.dropFirst())
        }
      case .whileStmt(let whileStmt):
        switch command {
        case .stepOver:
          annotatedEntries.append((command, nextStmt))
          remainingStmts = Array(remainingStmts.dropFirst())
        case .stepInto(branch: true):
          annotatedEntries.append((command, nextStmt))
          remainingStmts = whileStmt.body.body + remainingStmts
        case .stepInto(branch: false):
          annotatedEntries.append((command, nextStmt))
          remainingStmts = Array(remainingStmts.dropFirst())
        }
      }
    }
    self.entries = annotatedEntries
  }
}
