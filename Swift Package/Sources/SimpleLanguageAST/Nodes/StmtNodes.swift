/// An exhaustive enumeration of all known `Stmt` nodes.
public enum StmtEnum {
  case variableDecl(VariableDeclStmt)
  case assign(AssignStmt)
  case observe(ObserveStmt)
  case codeBlock(CodeBlockStmt)
  case ifStmt(IfStmt)
  case ifElse(IfElseStmt)
  case prob(ProbStmt)
  case whileStmt(WhileStmt)
}

public protocol Stmt: ASTNode {
  func accept<VisitorType: ASTVisitor>(_ visitor: VisitorType) -> VisitorType.StmtReturnType
  func accept<VisitorType: ASTVerifier>(_ visitor: VisitorType) throws -> VisitorType.StmtReturnType
  
  var asEnum: StmtEnum { get }
}

public extension Stmt {
  var asEnum: StmtEnum {
    switch (self) {
    case let varDecl as VariableDeclStmt:
      return .variableDecl(varDecl)
    case let assign as AssignStmt:
      return .assign(assign)
    case let observe as ObserveStmt:
      return .observe(observe)
    case let ifStmt as IfStmt:
      return .ifStmt(ifStmt)
    case let ifElse as IfElseStmt:
      return .ifElse(ifElse)
    case let prob as ProbStmt:
      return .prob(prob)
    case let whileStmt as WhileStmt:
      return .whileStmt(whileStmt)
    default:
      fatalError("Unknown stmt node")
    }
  }
}

public enum Type: CustomStringConvertible, Equatable {
  case int
  case bool
  
  public var description: String {
    switch self {
    case .int:
      return "int"
    case .bool:
      return "bool"
    }
  }
}

/// A declaration of a new variable. E.g. `int x = y + 2`
public struct VariableDeclStmt: Stmt {
  /// The variable that's being declared
  public let variable: SourceVariable
  /// The expression that's assigned to the variable
  public let expr: Expr

  public let nodeId: ASTNodeId
  public let range: SourceRange
  
  public init(variable: SourceVariable, expr: Expr, range: SourceRange, nodeId: ASTNodeId = .createNew()) {
    self.variable = variable
    self.expr = expr
    self.nodeId = nodeId
    self.range = range
  }
}

/// An assignment to an already declared variable. E.g. `x = x + 1`
public struct AssignStmt: Stmt {
  /// The variable that's being assigned a value
  public let variable: UnresolvedVariable
  /// The expression that's assigned to the variable
  public let expr: Expr
  
  public let nodeId: ASTNodeId
  public let range: SourceRange
  
  public init(variable: UnresolvedVariable, expr: Expr, range: SourceRange, nodeId: ASTNodeId = .createNew()) {
    self.variable = variable
    self.expr = expr
    self.nodeId = nodeId
    self.range = range
  }
}

public struct ObserveStmt: Stmt {
  public let condition: Expr
  
  public let nodeId: ASTNodeId
  public let range: SourceRange
  
  public init(condition: Expr, range: SourceRange, nodeId: ASTNodeId = .createNew()) {
    self.condition = condition
    self.nodeId = nodeId
    self.range = range
  }
}

/// A code block that contains multiple statements inside braces.
public struct CodeBlockStmt: Stmt {
  public let body: [Stmt]
  
  public let nodeId: ASTNodeId
  public let range: SourceRange
  
  public init(body: [Stmt], range: SourceRange, nodeId: ASTNodeId = .createNew()) {
    self.body = body
    self.nodeId = nodeId
    self.range = range
  }
}

public struct IfStmt: Stmt {
  public let condition: Expr
  public let body: CodeBlockStmt
  
  public let nodeId: ASTNodeId
  public let range: SourceRange
  
  public init(condition: Expr, body: CodeBlockStmt, range: SourceRange, nodeId: ASTNodeId = .createNew()) {
    self.condition = condition
    self.body = body
    self.nodeId = nodeId
    self.range = range
  }
}

public struct IfElseStmt: Stmt {
  public let condition: Expr
  public let ifBody: CodeBlockStmt
  public let elseBody: CodeBlockStmt
  
  public let nodeId: ASTNodeId
  public let range: SourceRange
  
  public init(condition: Expr, ifBody: CodeBlockStmt, elseBody: CodeBlockStmt, range: SourceRange, nodeId: ASTNodeId = .createNew()) {
    self.condition = condition
    self.ifBody = ifBody
    self.elseBody = elseBody
    self.nodeId = nodeId
    self.range = range
  }
}

public struct ProbStmt: Stmt {
  public let probability: Double
  public let ifBody: CodeBlockStmt
  public let elseBody: CodeBlockStmt?
  
  public let nodeId: ASTNodeId
  public let range: SourceRange
  
  public init(probability: Double, ifBody: CodeBlockStmt, elseBody: CodeBlockStmt?, range: SourceRange, nodeId: ASTNodeId = .createNew()) {
    self.probability = probability
    self.ifBody = ifBody
    self.elseBody = elseBody
    self.nodeId = nodeId
    self.range = range
  }
}

public struct WhileStmt: Stmt {
  public let condition: Expr
  public let body: CodeBlockStmt
  
  public let nodeId: ASTNodeId
  public let range: SourceRange
  
  public init(condition: Expr, body: CodeBlockStmt, range: SourceRange, nodeId: ASTNodeId = .createNew()) {
    self.condition = condition
    self.body = body
    self.nodeId = nodeId
    self.range = range
  }
}

// MARK: - AST Visitation

public extension VariableDeclStmt {
  func accept<VisitorType: ASTVisitor>(_ visitor: VisitorType) -> VisitorType.StmtReturnType {
    visitor.visit(self)
  }
  
  func accept<VisitorType: ASTVerifier>(_ visitor: VisitorType) throws -> VisitorType.StmtReturnType {
    try visitor.visit(self)
  }
  
  func accept<VisitorType: ASTRewriter>(_ visitor: VisitorType) throws -> Self {
    return try visitor.visit(self)
  }
}

public extension AssignStmt {
  func accept<VisitorType: ASTVisitor>(_ visitor: VisitorType) -> VisitorType.StmtReturnType {
    visitor.visit(self)
  }
  
  func accept<VisitorType: ASTVerifier>(_ visitor: VisitorType) throws -> VisitorType.StmtReturnType {
    try visitor.visit(self)
  }
  
  func accept<VisitorType: ASTRewriter>(_ visitor: VisitorType) throws -> Self {
    return try visitor.visit(self)
  }
}

public extension ObserveStmt {
  func accept<VisitorType: ASTVisitor>(_ visitor: VisitorType) -> VisitorType.StmtReturnType {
    visitor.visit(self)
  }
  
  func accept<VisitorType: ASTVerifier>(_ visitor: VisitorType) throws -> VisitorType.StmtReturnType {
    try visitor.visit(self)
  }
  
  func accept<VisitorType: ASTRewriter>(_ visitor: VisitorType) throws -> Self {
    return try visitor.visit(self)
  }
}

public extension CodeBlockStmt {
  func accept<VisitorType: ASTVisitor>(_ visitor: VisitorType) -> VisitorType.StmtReturnType {
    visitor.visit(self)
  }
  
  func accept<VisitorType: ASTVerifier>(_ visitor: VisitorType) throws -> VisitorType.StmtReturnType {
    try visitor.visit(self)
  }
  
  func accept<VisitorType: ASTRewriter>(_ visitor: VisitorType) throws -> Self {
    return try visitor.visit(self)
  }
}

public extension IfStmt {
  func accept<VisitorType: ASTVisitor>(_ visitor: VisitorType) -> VisitorType.StmtReturnType {
    visitor.visit(self)
  }
  
  func accept<VisitorType: ASTVerifier>(_ visitor: VisitorType) throws -> VisitorType.StmtReturnType {
    try visitor.visit(self)
  }
  
  func accept<VisitorType: ASTRewriter>(_ visitor: VisitorType) throws -> Self {
    return try visitor.visit(self)
  }
}

public extension IfElseStmt {
  func accept<VisitorType: ASTVisitor>(_ visitor: VisitorType) -> VisitorType.StmtReturnType {
    visitor.visit(self)
  }
  
  func accept<VisitorType: ASTVerifier>(_ visitor: VisitorType) throws -> VisitorType.StmtReturnType {
    try visitor.visit(self)
  }
  
  func accept<VisitorType: ASTRewriter>(_ visitor: VisitorType) throws -> Self {
    return try visitor.visit(self)
  }
}

public extension ProbStmt {
  func accept<VisitorType: ASTVisitor>(_ visitor: VisitorType) -> VisitorType.StmtReturnType {
    visitor.visit(self)
  }
  
  func accept<VisitorType: ASTVerifier>(_ visitor: VisitorType) throws -> VisitorType.StmtReturnType {
    try visitor.visit(self)
  }
  
  func accept<VisitorType: ASTRewriter>(_ visitor: VisitorType) throws -> Self {
    return try visitor.visit(self)
  }
}

public extension WhileStmt {
  func accept<VisitorType: ASTVisitor>(_ visitor: VisitorType) -> VisitorType.StmtReturnType {
    visitor.visit(self)
  }
  
  func accept<VisitorType: ASTVerifier>(_ visitor: VisitorType) throws -> VisitorType.StmtReturnType {
    try visitor.visit(self)
  }
  
  func accept<VisitorType: ASTRewriter>(_ visitor: VisitorType) throws -> Self {
    return try visitor.visit(self)
  }
}

// MARK: - Equality ignoring ranges

extension VariableDeclStmt {
  public func equalsIgnoringRange(other: ASTNode) -> Bool {
    guard let other = other as? VariableDeclStmt else {
      return false
    }
    return self.variable == other.variable &&
      self.expr.equalsIgnoringRange(other: other.expr)
  }
}

extension AssignStmt {
  public func equalsIgnoringRange(other: ASTNode) -> Bool {
    guard let other = other as? AssignStmt else {
      return false
    }
    return self.variable.hasSameName(as: other.variable) &&
      self.expr.equalsIgnoringRange(other: other.expr)
  }
}

extension ObserveStmt {
  public func equalsIgnoringRange(other: ASTNode) -> Bool {
    guard let other = other as? ObserveStmt else {
      return false
    }
    return self.condition.equalsIgnoringRange(other: other.condition)
  }
}

extension CodeBlockStmt {
  public func equalsIgnoringRange(other: ASTNode) -> Bool {
    guard let other = other as? CodeBlockStmt else {
      return false
    }
    if self.body.count != other.body.count {
      return false
    }
    return zip(self.body, other.body).allSatisfy({
      $0.0.equalsIgnoringRange(other: $0.1)
    })
  }
}

extension IfStmt {
  public func equalsIgnoringRange(other: ASTNode) -> Bool {
    guard let other = other as? IfStmt else {
      return false
    }
    return self.condition.equalsIgnoringRange(other: other.condition) &&
      self.body.equalsIgnoringRange(other: other.body)
  }
}

extension IfElseStmt {
  public func equalsIgnoringRange(other: ASTNode) -> Bool {
    guard let other = other as? IfElseStmt else {
      return false
    }
    return self.condition.equalsIgnoringRange(other: other.condition) &&
      self.ifBody.equalsIgnoringRange(other: other.ifBody) &&
      self.elseBody.equalsIgnoringRange(other: other.elseBody)
  }
}

extension ProbStmt {
  public func equalsIgnoringRange(other: ASTNode) -> Bool {
    guard let other = other as? ProbStmt else {
      return false
    }
    if self.probability != other.probability {
      return false
    }
    if !self.ifBody.equalsIgnoringRange(other: other.ifBody) {
      return false
    }
    
    switch (self.elseBody, other.elseBody) {
    case (let selfElseBody?, let otherElseBody?):
      return selfElseBody.equalsIgnoringRange(other: otherElseBody)
    case (nil, nil):
      return true
    default:
      return false
    }
  }
}

extension WhileStmt {
  public func equalsIgnoringRange(other: ASTNode) -> Bool {
    guard let other = other as? WhileStmt else {
      return false
    }
    return self.condition.equalsIgnoringRange(other: other.condition) &&
      self.body.equalsIgnoringRange(other: other.body)
  }
}
