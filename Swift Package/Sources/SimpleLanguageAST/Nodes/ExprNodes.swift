/// An exhaustive enumeration of all known `Expr` nodes.
public enum ExprEnum {
  case binaryOperator(BinaryOperatorExpr)
  case integerLiteral(IntegerLiteralExpr)
  case boolLiteral(BoolLiteralExpr)
  case variableReference(VariableReferenceExpr)
  case paren(ParenExpr)
}

public protocol Expr: ASTNode {
  func accept<VisitorType: ASTVisitor>(_ visitor: VisitorType) -> VisitorType.ExprReturnType
  func accept<VisitorType: ASTVerifier>(_ visitor: VisitorType) throws -> VisitorType.ExprReturnType
  
  var asEnum: ExprEnum { get }
}

public extension Expr {
  var asEnum: ExprEnum {
    switch self {
    case let binaryOperator as BinaryOperatorExpr:
      return .binaryOperator(binaryOperator)
    case let integerLiteral as IntegerLiteralExpr:
      return .integerLiteral(integerLiteral)
    case let boolLiteral as BoolLiteralExpr:
      return .boolLiteral(boolLiteral)
    case let variableReference as VariableReferenceExpr:
      return .variableReference(variableReference)
    case let paren as ParenExpr:
      return .paren(paren)
    default:
      fatalError("Unknown Expr node")
    }
  }
}

// MARK: - Expression nodes

public enum BinaryOperator {
  case plus
  case minus
  case equal
  case lessThan
  
  /// Returns the precedence of the operator. A greater value means higher precedence.
  public var precedence: Int {
    switch self {
    case .plus, .minus:
      return 2
    case .equal, .lessThan:
      return 1
    }
  }
}

/// Application of a binary operator to two operands
public struct BinaryOperatorExpr: Expr {
  public let lhs: Expr
  public let rhs: Expr
  public let `operator`: BinaryOperator

  public let nodeId: ASTNodeId
  public let range: SourceRange
 
  public init(lhs: Expr, operator op: BinaryOperator, rhs: Expr, range: SourceRange, nodeId: ASTNodeId = .createNew()) {
    self.lhs = lhs
    self.rhs = rhs
    self.operator = op
    self.nodeId = nodeId
    self.range = range
  }
}

/// An interger literal
public struct IntegerLiteralExpr: Expr {
  public let value: Int

  public let nodeId: ASTNodeId
  public let range: SourceRange
  
  public init(value: Int, range: SourceRange, nodeId: ASTNodeId = .createNew()) {
    self.value = value
    self.nodeId = nodeId
    self.range = range
  }
}

public struct BoolLiteralExpr: Expr {
  public let value: Bool

  public let nodeId: ASTNodeId
  public let range: SourceRange
  
  public init(value: Bool, range: SourceRange, nodeId: ASTNodeId = .createNew()) {
    self.value = value
    self.nodeId = nodeId
    self.range = range
  }
}

/// A reference to a variable.
public struct VariableReferenceExpr: Expr {
  /// Before type checking (in particular variable resolving), `variable` is `.unresolved`, afterwards it is always `resolved`
  public let variable: UnresolvedVariable

  public let nodeId: ASTNodeId
  public let range: SourceRange
  
  public init(variable: UnresolvedVariable, range: SourceRange, nodeId: ASTNodeId = .createNew()) {
    self.variable = variable
    self.nodeId = nodeId
    self.range = range
  }
}

/// An expression inside paranthesis
public struct ParenExpr: Expr {
  public let subExpr: Expr

  public let nodeId: ASTNodeId
  public let range: SourceRange
  
  public init(subExpr: Expr, range: SourceRange, nodeId: ASTNodeId = .createNew()) {
    self.subExpr = subExpr
    self.nodeId = nodeId
    self.range = range
  }
}

// MARK: - Visitation


public extension BinaryOperatorExpr {
  func accept<VisitorType: ASTVisitor>(_ visitor: VisitorType) -> VisitorType.ExprReturnType {
    visitor.visit(self)
  }
  
  func accept<VisitorType: ASTVerifier>(_ visitor: VisitorType) throws -> VisitorType.ExprReturnType {
    try visitor.visit(self)
  }
  
  func accept<VisitorType: ASTRewriter>(_ visitor: VisitorType) throws -> Self {
    return try visitor.visit(self)
  }
}

public extension IntegerLiteralExpr {
  func accept<VisitorType: ASTVisitor>(_ visitor: VisitorType) -> VisitorType.ExprReturnType {
    visitor.visit(self)
  }
  
  func accept<VisitorType: ASTVerifier>(_ visitor: VisitorType) throws -> VisitorType.ExprReturnType {
    try visitor.visit(self)
  }
  
  func accept<VisitorType: ASTRewriter>(_ visitor: VisitorType) throws -> Self {
    return try visitor.visit(self)
  }
}

public extension BoolLiteralExpr {
  func accept<VisitorType: ASTVisitor>(_ visitor: VisitorType) -> VisitorType.ExprReturnType {
    visitor.visit(self)
  }
  
  func accept<VisitorType: ASTVerifier>(_ visitor: VisitorType) throws -> VisitorType.ExprReturnType {
    try visitor.visit(self)
  }
  
  func accept<VisitorType: ASTRewriter>(_ visitor: VisitorType) throws -> Self {
    return try visitor.visit(self)
  }
}

public extension VariableReferenceExpr {
  func accept<VisitorType: ASTVisitor>(_ visitor: VisitorType) -> VisitorType.ExprReturnType {
    visitor.visit(self)
  }
  
  func accept<VisitorType: ASTVerifier>(_ visitor: VisitorType) throws -> VisitorType.ExprReturnType {
    try visitor.visit(self)
  }
  
  func accept<VisitorType: ASTRewriter>(_ visitor: VisitorType) throws -> Self {
    return try visitor.visit(self)
  }
}

public extension ParenExpr {
  func accept<VisitorType: ASTVisitor>(_ visitor: VisitorType) -> VisitorType.ExprReturnType {
    visitor.visit(self)
  }
  
  func accept<VisitorType: ASTVerifier>(_ visitor: VisitorType) throws -> VisitorType.ExprReturnType {
    try visitor.visit(self)
  }
  
  func accept<VisitorType: ASTRewriter>(_ visitor: VisitorType) throws -> Self {
    return try visitor.visit(self)
  }
}

// MARK: - Equality ignoring ranges


public extension BinaryOperatorExpr {
  func equalsIgnoringRange(other: ASTNode) -> Bool {
    guard let other = other as? BinaryOperatorExpr else {
      return false
    }
    return self.operator == other.operator &&
      self.lhs.equalsIgnoringRange(other: other.lhs) &&
      self.rhs.equalsIgnoringRange(other: other.rhs)
  }
}

public extension IntegerLiteralExpr {
  func equalsIgnoringRange(other: ASTNode) -> Bool {
    guard let other = other as? IntegerLiteralExpr else {
      return false
    }
    return self.value == other.value
  }
}

public extension BoolLiteralExpr {
  func equalsIgnoringRange(other: ASTNode) -> Bool {
    guard let other = other as? BoolLiteralExpr else {
      return false
    }
    return self.value == other.value
  }
}

public extension VariableReferenceExpr {
  func equalsIgnoringRange(other: ASTNode) -> Bool {
    guard let other = other as? VariableReferenceExpr else {
      return false
    }
    return self.variable.hasSameName(as: other.variable)
  }
}

public extension ParenExpr {
  func equalsIgnoringRange(other: ASTNode) -> Bool {
    guard let other = other as? ParenExpr else {
      return false
    }
    return self.subExpr.equalsIgnoringRange(other: other.subExpr)
  }
}
