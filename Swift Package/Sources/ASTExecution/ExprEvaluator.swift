import SimpleLanguageAST

enum ExprEvaluator {
  /// For each of the given `samples`, evaluate the value of the given `expr`.
  /// The resulting variable values are in the same order as the `samples`.
  static func evaluateExpr(_ expr: Expr, samples: [ASTSample]) -> [VariableValue] {
    switch expr.asEnum {
    case .binaryOperator(let binary):
      return evalute(binary: binary, samples: samples)
    case .integerLiteral(let integerLiteral):
      return Array(repeating: .integer(integerLiteral.value), count: samples.count)
    case .boolLiteral(let boolLiteral):
      return Array(repeating: .bool(boolLiteral.value), count: samples.count)
    case .variableReference(let varRef):
      return samples.map { sample in
        guard case .resolved(let variable) = varRef.variable else {
          fatalError("Variable not resolved before execution? Did type checking miss it?")
        }
        guard let value = sample.values[variable] else {
          fatalError("Variable referenced before use?")
        }
        return value
      }
    case .paren(let parenExpr):
      return evaluateExpr(parenExpr.subExpr, samples: samples)
    }
  }

  private static func evalute(binary: BinaryOperatorExpr, samples: [ASTSample]) -> [VariableValue] {
    let lhs = evaluateExpr(binary.lhs, samples: samples)
    let rhs = evaluateExpr(binary.rhs, samples: samples)
    switch binary.operator {
    case .plus:
      return zip(lhs, rhs).map{ (lhs, rhs) in
        switch (lhs, rhs) {
        case (.integer(let lhsValue), .integer(let rhsValue)):
          return .integer(lhsValue + rhsValue)
        default:
          fatalError("Addition of these variable types is not supported")
        }
      }
    case .minus:
      return zip(lhs, rhs).map{ (lhs, rhs) in
        switch (lhs, rhs) {
        case (.integer(let lhsValue), .integer(let rhsValue)):
          return .integer(lhsValue - rhsValue)
        default:
          fatalError("Subtraction of these variable types is not supported")
        }
      }
    case .equal:
      return zip(lhs, rhs).map{ (lhs, rhs) in
        switch (lhs, rhs) {
        case (.integer(let lhsValue), .integer(let rhsValue)):
          return .bool(lhsValue == rhsValue)
        case (.bool(let lhsValue), .bool(let rhsValue)):
          return .bool(lhsValue == rhsValue)
        default:
          fatalError("Comparison of different variable types is not supported")
        }
      }
    case .lessThan:
      return zip(lhs, rhs).map{ (lhs, rhs) in
        switch (lhs, rhs) {
        case (.integer(let lhsValue), .integer(let rhsValue)):
          return .bool(lhsValue < rhsValue)
        default:
          fatalError("Comparison of these variable types is not supported")
        }
      }
    }
  }
}
