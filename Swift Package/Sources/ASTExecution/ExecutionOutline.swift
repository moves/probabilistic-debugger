/// The execution outline represent all possible runs of a program.
/// For example the following program is represented by the following execution outline:
/// ```
/// int x = 5
/// while 1 < x {
///   x = x - 1
/// }
/// ```
///
/// Execution outline:
/// ```
/// ▷ int x = 5, 1000 samples
/// ▽ while 1 < x, 1000 samples
///   ▽ Iteration 1
///     ▷ x = x - 1, 750 samples
///   ▽ Iteration 2
///     ▷ x = x - 1, 500 samples
///   ▽ Iteration 3
///     ▷ x = x - 1, 250 samples
/// ▷ end, 1000 samples
/// ```
///
/// For each entry in the execution outline, the `ASTExecutionState` is stored, so that a debugger may be directly pointed to that location.
public struct ExecutionOutline: ExpressibleByArrayLiteral, CustomStringConvertible {
  public typealias ArrayLiteralElement = ExecutionOutlineEntry
  
  public let entries: [ExecutionOutlineEntry]
  
  internal init(_ entries: [ExecutionOutlineEntry]) {
    self.entries = entries
  }
  
  public init(arrayLiteral elements: ExecutionOutlineEntry...) {
    self.entries = elements
  }
}

public enum ExecutionOutlineEntry: CustomStringConvertible {
  /// A normal statement that transfers conrol flow to the next statement.
  /// This might still be an `observe` statement and filter out some or all samples
  case statement(state: ASTExecutionState)
  
  /// A state that ends some context. The context could be a loop iteration or an entire program
  case end(state: ASTExecutionState)
  
  /// An if/else branch. Not both branches need to be viable. If a branch is not viable it is `nil`.
  case branch(state: ASTExecutionState, true: ExecutionOutline?, false: ExecutionOutline?)
  
  /// A loop. Each iteration is represented by an `ExecutionOutline`, consisting of the statements that were executed in the loop body.
  /// The exit states represent the states that are reached after executing **at most** 0, 1, etc. iterations.
  case loop(state: ASTExecutionState, iterations: [ExecutionOutline], exitStates: [ASTExecutionState])
  
  public var state: ASTExecutionState {
    switch self {
    case .statement(state: let state):
      return state
    case .end(state: let state):
      return state
    case .branch(state: let state, true: _, false: _):
      return state
    case .loop(state: let state, iterations: _, exitStates: _):
      return state
    }
  }
}

// MARK: - Descriptions

extension ExecutionOutline {
  public var description: String {
    return description(sourceCode: nil)
  }
  
  public func description(sourceCode: String?) -> String {
    return entries.map({
      return $0.description(sourceCode: sourceCode)
    }).joined(separator: "\n")
  }
}

extension ExecutionOutlineEntry {
  public var description: String {
    return description(sourceCode: nil)
  }
  
  public func description(sourceCode: String?) -> String {
    
    switch self {
    case .statement(state: let state):
      return "▷ \(state.description(sourceCode: sourceCode))"
    case .end(state: let state):
      return "▷ end, \(state.samples.count) samples"
    case .branch(state: let state, true: let trueBranch, false: let falseBranch):
      var descriptionPieces: [String] = []
      descriptionPieces.append("▽ \(state.description(sourceCode: sourceCode))")
      if let trueBranch = trueBranch {
        descriptionPieces.append("""
          ▽ true-Branch
        \(trueBranch.description(sourceCode: sourceCode).indented(2))
        """)
      }
      if let falseBranch = falseBranch {
        descriptionPieces.append("""
          ▽ false-Branch
        \(falseBranch.description(sourceCode: sourceCode).indented(2))
        """)
      }
      return descriptionPieces.joined(separator: "\n")
    case .loop(state: let state, iterations: let iterations, exitStates: let exitStates):
      var descriptionPieces: [String] = []
      descriptionPieces.append("▽ \(state.description(sourceCode: sourceCode))")
      for (index, iteration) in iterations.enumerated() {
        descriptionPieces.append("""
            ▽ Iteration \(index + 1)
          \(iteration.description(sourceCode: sourceCode).indented(2))
          """)
      }
      for (index, exitState) in exitStates.enumerated() {
        descriptionPieces.append("""
            ▽ Exit after at most \(index) iterations
              ▷ \(exitState.description(sourceCode: sourceCode))
          """)
      }
      return descriptionPieces.joined(separator: "\n")
    }
  }
}

extension ASTExecutionState {
  public func description(sourceCode: String?) -> String {
    let statementDescription: String
    if let nextStmt = remainingStmts.first {
      if let sourceCode = sourceCode {
        statementDescription = String(sourceCode.split(separator: "\n")[nextStmt.range.lowerBound.line - 1]).trimmingCharacters(in: .whitespaces)
      } else {
        statementDescription = nextStmt.range.lowerBound.description
      }
    } else {
      statementDescription = "end"
    }
    return "\(statementDescription), \(samples.count) samples"
  }
}
