import SimpleLanguageAST

fileprivate extension Array {
  /// Returns the second element of an array or `nil` if the array doesn't contain two elements.
  var second: Element? {
    if count >= 2 {
      return self[1]
    } else {
      return nil
    }
  }
}

extension ASTExecutionState {
  func splitExecutionAtBranchingStmt(condition: Expr, ifBlock: [Stmt], elseBlock: [Stmt], loopId: ASTNodeId? = nil) -> (ifTrueState: ASTExecutionState, ifFalseState: ASTExecutionState) {
    // Evaluate the condition once and use it to split all sample into exactly two branches.
    // If we used `executeExactlyOneCommand(.stepInto(true))` and `executeExactlyOneCommand(.stepInto(false))`
    // to generate the two execution branches and the condition is not deterministic, we might end up
    // with samples ending up in both or none of the two branches.
    let conditionVarValues = ExprEvaluator.evaluateExpr(condition, samples: samples)
    let conditionValues = conditionVarValues.map { varValue -> Bool in
      guard case .bool(let boolValue) = varValue else {
        fatalError("Observe condition did not evaluate to Bool?")
      }
      return boolValue
    }
    let ifTrueState = executeBranchingStmt(
      ifBlock: ifBlock,
      elseBlock: elseBlock,
      command: .stepInto(branch: true),
      conditionValues: conditionValues,
      loopId: loopId
    )
    let ifFalseState = executeBranchingStmt(
      ifBlock: ifBlock,
      elseBlock: elseBlock,
      command: .stepInto(branch: false),
      conditionValues: conditionValues,
      loopId: loopId
    )
    return (ifTrueState, ifFalseState)
  }
}

public enum ExecutionOutlineGenerator {
  /// Generate the execution outline of executing `stmts`.
  public static func generateExecutionOutline(
    stmts: [Stmt],
    numSamples: Int
  ) -> ExecutionOutline {
    let initialState = ASTExecutionState(
      remainingStmts: stmts,
      executionHistory: [],
      samples: Array(repeating: ASTSample(values: [:]), count: numSamples),
      finishedLoopUnrolls: .empty,
      inProgressLoopUnrolls: .empty
    )
    return generateExecutionOutline(executionState: initialState).outline
  }
  
  /// Generate the execution outline of executing `executionState` until either a node with ID `stopAtNode` or the end of the program is reached.
  private static func generateExecutionOutline(
    executionState: ASTExecutionState,
    stopAtNode: ASTNodeId? = nil
  ) -> (outline: ExecutionOutline, finalState: ASTExecutionState) {
    var outlineEntries: [ExecutionOutlineEntry] = []
    var currentState = executionState
    while let nextStmt = currentState.remainingStmts.first, nextStmt.nodeId != stopAtNode {
      switch nextStmt.asEnum {
      case .variableDecl, .assign, .observe:
        outlineEntries.append(.statement(state: currentState))
        currentState = currentState.executeOneCommand(.stepOver)
      case .codeBlock:
        outlineEntries.append(.statement(state: currentState))
        currentState = currentState.executeOneCommand(.stepOver)
      case .ifStmt, .ifElse:
        let condition: Expr
        let ifBlock: [Stmt]
        let elseBlock: [Stmt]
        if let ifStmt = nextStmt as? IfStmt {
          condition = ifStmt.condition
          ifBlock = ifStmt.body.body
          elseBlock = []
        } else if let ifElseStmt = nextStmt as? IfElseStmt {
          condition = ifElseStmt.condition
          ifBlock = ifElseStmt.ifBody.body
          elseBlock = ifElseStmt.elseBody.body
        } else {
          fatalError("We should only have IfStmt or IfElseStmt here")
        }
        
        let (ifState, elseState) = currentState.splitExecutionAtBranchingStmt(
          condition: condition,
          ifBlock: ifBlock,
          elseBlock: elseBlock
        )
        let (ifOutline, ifExitState) = generateExecutionOutline(
          executionState: ifState,
          stopAtNode: currentState.remainingStmts.second?.nodeId // Stop at the statement following the `if`-statement
        )
        let (elseOutline, elseExitState) = generateExecutionOutline(
          executionState: elseState,
          stopAtNode: currentState.remainingStmts.second?.nodeId // Stop at the statement following the `if`-statement
        )
        
        let includeElseOutline = (nextStmt is IfElseStmt)
        outlineEntries.append(
          .branch(
            state: currentState,
            true: ifOutline,
            false: includeElseOutline ? elseOutline : nil
          )
        )
        
        assert(ifState.inProgressLoopUnrolls == elseState.inProgressLoopUnrolls, "The if and else branch should not open different loops")
        
        // Merge the samples generated from executing the `if` and the `else` branch.
        currentState = ASTExecutionState(
          remainingStmts: Array(currentState.remainingStmts.dropFirst()),
          executionHistory: currentState.executionHistory + [.stepOver],
          samples: ifExitState.samples + elseExitState.samples,
          finishedLoopUnrolls: LoopUnrolls.merging(ifState.finishedLoopUnrolls, elseState.finishedLoopUnrolls),
          inProgressLoopUnrolls: ifState.inProgressLoopUnrolls
        )
      case .prob(let probStmt):
        let diceThrows = currentState.samples.map({ _ in
          return Double.random(in: 0..<1) < probStmt.probability
        })
        
        let ifState = currentState.executeBranchingStmt(
          ifBlock: probStmt.ifBody.body,
          elseBlock: probStmt.elseBody?.body ?? [],
          command: .stepInto(branch: true),
          conditionValues: diceThrows
        )
        let (ifOutline, ifExitState) = generateExecutionOutline(
          executionState: ifState,
          stopAtNode: currentState.remainingStmts.second?.nodeId // Stop at the statement following the `if`-statement
        )
        let elseState = currentState.executeBranchingStmt(
          ifBlock: probStmt.ifBody.body,
          elseBlock: probStmt.elseBody?.body ?? [],
          command: .stepInto(branch: false),
          conditionValues: diceThrows
        )
        let (elseOutline, elseExitState) = generateExecutionOutline(
          executionState: elseState,
          stopAtNode: currentState.remainingStmts.second?.nodeId // Stop at the statement following the `if`-statement
        )
        
        outlineEntries.append(
          .branch(
            state: currentState,
            true: ifOutline,
            false: (probStmt.elseBody != nil) ? elseOutline : nil
          )
        )
        
        assert(ifState.inProgressLoopUnrolls == elseState.inProgressLoopUnrolls, "The if and else branch should not open different loops")
        
        // Merge the samples generated from executing the `if` and the `else` branch.
        currentState = ASTExecutionState(
          remainingStmts: Array(currentState.remainingStmts.dropFirst()),
          executionHistory: currentState.executionHistory + [.stepOver],
          samples: ifExitState.samples + elseExitState.samples,
          finishedLoopUnrolls: LoopUnrolls.merging(ifState.finishedLoopUnrolls, elseState.finishedLoopUnrolls),
          inProgressLoopUnrolls: ifState.inProgressLoopUnrolls
        )
      case .whileStmt(let whileStmt):
        let (outlineEntry, finalState) = generateOutlineForLoop(whileStmt, loopEntryState: currentState)
        outlineEntries.append(outlineEntry)
        currentState = finalState
      }
    }
    outlineEntries.append(.end(state: currentState))
    return (ExecutionOutline(outlineEntries), currentState)
  }
  
  /// Generate the `ExecutionOutlineEntry` for executing the given `whileStmt` starting with `loopEntryState`.
  private static func generateOutlineForLoop(
    _ whileStmt: WhileStmt,
    loopEntryState: ASTExecutionState
  ) -> (outlineEntry: ExecutionOutlineEntry, finalState: ASTExecutionState) {
    var iterations: [ExecutionOutline] = []
    var exitStates: [ASTExecutionState] = []
    var currentState = loopEntryState
    
    while !currentState.samples.isEmpty {
      assert(currentState.remainingStmts.first?.nodeId == whileStmt.nodeId)
      
      let (iterationEntryState, loopFinishedState) = currentState.splitExecutionAtBranchingStmt(
        condition: whileStmt.condition,
        ifBlock: whileStmt.body.body + [whileStmt],
        elseBlock: [],
        loopId: whileStmt.nodeId
      )
      
      let mergedLoopUnrolls: LoopUnrolls
      if let lastExitState = exitStates.last {
        mergedLoopUnrolls = .merging(loopFinishedState.finishedLoopUnrolls, lastExitState.finishedLoopUnrolls)
        assert(lastExitState.inProgressLoopUnrolls == loopFinishedState.inProgressLoopUnrolls, "Executing a loop shouldn't affect any in progress loops")
      } else {
        mergedLoopUnrolls = loopFinishedState.finishedLoopUnrolls
      }
      let mergedExitState = ASTExecutionState(
        remainingStmts: Array(loopEntryState.remainingStmts.dropFirst()),
        executionHistory: loopEntryState.executionHistory + [.stepOver],
        samples: loopFinishedState.samples + (exitStates.last?.samples ?? []),
        finishedLoopUnrolls: mergedLoopUnrolls,
        inProgressLoopUnrolls: loopFinishedState.inProgressLoopUnrolls
      )
      exitStates.append(mergedExitState)
      
      if iterationEntryState.samples.isEmpty {
        break
      }
      let (outline, iterationFinishedState) = generateExecutionOutline(executionState: iterationEntryState, stopAtNode: whileStmt.nodeId)
      iterations.append(outline)
      currentState = iterationFinishedState
    }
    // If we don't have any exit states, then we didn't have any samples to execute in the loop.
    // Generate an outline by just stepping over the loop.
    let finalState = exitStates.last ?? loopEntryState.executeOneCommand(.stepOver)

    return (.loop(state: loopEntryState, iterations: iterations, exitStates: exitStates), finalState)
  }
}
