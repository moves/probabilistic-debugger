import SimpleLanguageAST

/// Describes how many times a `while`-statement's (identified by the
/// `while`-statements' `ASTNodeId`) has been executed during sampling-based
/// execution.
public struct LoopUnrolls: Equatable {
  public let values: [ASTNodeId: Int]

  public static let empty: LoopUnrolls = LoopUnrolls(values: [:])

  /// Merge the given loop unrolls, taking the maximum loop iteration count for
  /// loops defined in both unrolls.
  public static func merging(_ lhs: LoopUnrolls, _ rhs: LoopUnrolls) -> LoopUnrolls {
    let mergedValues = lhs.values.merging(rhs.values, uniquingKeysWith: {
      max($0, $1)
    })
    return LoopUnrolls(values: mergedValues)
  }

  /// Increase the loop unroll count of the loop with the given `loopId` by one.
  /// If `loopId` is `nil`, this is a no-op.
  public func increasingUnrollCount(of loopId: ASTNodeId?) -> LoopUnrolls {
    guard let loopId = loopId else {
      return self
    }
    var newValues = values
    newValues[loopId, default: 0] += 1
    return LoopUnrolls(values: newValues)
  }

  /// Increase the loop unroll count of `loopId` to the count in
  /// `inProgressCount` if it is larger, otherwise a no-op.
  /// If `loopId` is `nil`, this is a no-op.
  public func commitUnrollCount(of loopId: ASTNodeId?, fromInProgressCount inProgressCount: LoopUnrolls) -> LoopUnrolls {
    guard let loopId = loopId else {
      return self
    }
    var newValues = values
    newValues[loopId] = max(inProgressCount.values[loopId] ?? 0, newValues[loopId, default: 0])
    return LoopUnrolls(values: newValues)
  }

  /// Clear the unroll count of `loopId`.
  /// If `loopId` is `nil`, this is a no-op.
  public func clearingUnrollCount(of loopId: ASTNodeId?) -> LoopUnrolls {
    guard let loopId = loopId else {
      return self
    }
    var newValues = values
    newValues[loopId] = nil
    return LoopUnrolls(values: newValues)
  }
}
