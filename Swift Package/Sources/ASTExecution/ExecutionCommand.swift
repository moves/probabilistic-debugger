/// Tells the `ASTExecutor` how a given statement should be executed. These are
/// essentially the debugger commands that control execution of a program.
public enum ExecutionCommand: Hashable, CustomStringConvertible {
  /// Step over the current statement
  case stepOver

  /// If the current statement is branching (`if`- or `while`-statement), jump
  /// into the given branch.
  /// If `branch == true`, jump into the `if`-branch of an `if`-statement and
  /// into the loop body of a `while`-statement.
  /// /// If `branch == true`, jump into the `else`-branch of an `if`-statement
  /// and terminate iteration of of a `while`-statement.
  case stepInto(branch: Bool)
  
  public var description: String {
    switch self {
    case .stepOver:
      return "step over"
    case .stepInto(branch: let branch):
      return "step into \(branch)"
    }
  }
}
