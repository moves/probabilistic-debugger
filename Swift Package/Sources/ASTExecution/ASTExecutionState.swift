import SimpleLanguageAST

/// Describes an intermediate state of execution during sample-based execution.
public struct ASTExecutionState: CustomStringConvertible {
  /// The statements that still need to be executed. In the initial execution
  /// state, this contains all top-level statements of the file.
  public let remainingStmts: [Stmt]

  /// The debugger execution commands that remain to be executed.
  public let executionHistory: [ExecutionCommand]

  /// The samples that describe the current variable values.
  public let samples: [ASTSample]

  /// After we have exited a loop, counts the maximum number of times a loop's
  /// body has been executed per execution of the loop.
  public let finishedLoopUnrolls: LoopUnrolls

  /// Counts the number of times a loop, which is currently being executed,
  /// has been unrolled. After the loops has finished executing, it the value
  /// computed in this dictionary is commited to `finishedLoopUnrolls` and
  /// cleared from this one.
  let inProgressLoopUnrolls: LoopUnrolls

  public var liveVariables: [SourceVariable] {
    // FIXME: We should be keeping track of live variables in a dedicated data
    // structure in which we clear them as variables go out of scope.
    guard let sample = samples.first else {
      return []
    }
    return Array(sample.values.keys)
  }
  
  /// Create the initial execution state for executing a program with the given `stmts`.
  public static func initialState(for stmts: [Stmt], numSamples: Int) -> ASTExecutionState {
    return ASTExecutionState(
      remainingStmts: stmts,
      executionHistory: [],
      samples: Array(repeating: ASTSample(values: [:]), count: numSamples),
      finishedLoopUnrolls: .empty,
      inProgressLoopUnrolls: .empty
    )
  }
  
  /// Executes this execution state until there are no more statements to
  /// execute.
  public func executeUntilEnd() -> ASTExecutionState {
    var currentState = self
    while !currentState.remainingStmts.isEmpty && !currentState.samples.isEmpty {
      currentState = currentState.executeOneCommand(.stepOver)
    }
    return currentState
  }

  /// Execute a single statements as described by the topmost execution command.
  public func executeOneCommand(_ command: ExecutionCommand) -> ASTExecutionState {
    switch remainingStmts.first?.asEnum {
    case .variableDecl(let varDecl):
      return executeAssignment(
        variable: varDecl.variable,
        expr: varDecl.expr,
        command: command
      )
    case .assign(let assign):
      guard case .resolved(let variable) = assign.variable else {
        fatalError("Variable not resolved as part of type checking?")
      }
      return executeAssignment(
        variable: variable,
        expr: assign.expr,
        command: command
      )
    case .observe(let observe):
      return executeObserveStmt(
        condition: observe.condition,
        command: command
      )
    case .codeBlock(let codeBlock):
      let unfoldedCodeBlock = ASTExecutionState(
        remainingStmts: codeBlock.body + remainingStmts,
        executionHistory: executionHistory,
        samples: samples,
        finishedLoopUnrolls: finishedLoopUnrolls,
        inProgressLoopUnrolls: inProgressLoopUnrolls
      )
      return unfoldedCodeBlock.executeOneCommand(command)
    case .ifStmt(let ifStmt):
      return executeBranchingStmt(
        condition: ifStmt.condition,
        ifBlock: ifStmt.body.body,
        elseBlock: [],
        command: command
      )
    case .ifElse(let ifElseStmt):
      return executeBranchingStmt(
        condition: ifElseStmt.condition,
        ifBlock: ifElseStmt.ifBody.body,
        elseBlock: ifElseStmt.elseBody.body,
        command: command
      )
    case .prob(let probStmt):
      let diceThrows = samples.map({ _ in
        return Double.random(in: 0..<1) < probStmt.probability
      })
      return executeBranchingStmt(
        ifBlock: probStmt.ifBody.body,
        elseBlock: probStmt.elseBody?.body ?? [],
        command: command,
        conditionValues: diceThrows
      )
    case .whileStmt(let whileStmt):
      /// Unroll one iteration of the loop.
      /// ```
      /// while cond {
      ///   body
      /// }
      /// rest
      /// ```
      /// becomes
      /// ```
      /// if cond {
      ///   body
      ///   while cond {
      ///     body
      ///   }
      /// }
      /// rest
      /// ```
      return executeBranchingStmt(
        condition: whileStmt.condition,
        ifBlock: whileStmt.body.body + [whileStmt],
        elseBlock: [],
        command: command,
        loopId: whileStmt.nodeId
      )
    case nil:
      fatalError("No more statement to execute?")
    }
  }

  private func executeAssignment(
    variable: SourceVariable,
    expr: Expr,
    command: ExecutionCommand
  ) -> ASTExecutionState {
    let assignValues = ExprEvaluator.evaluateExpr(expr, samples: samples)
    let newSamples = zip(samples, assignValues).map { (sample, value) in
      return sample.assigning(variable: variable, value: value)
    }

    return ASTExecutionState(
      remainingStmts: Array(remainingStmts.dropFirst()),
      executionHistory: executionHistory + [command],
      samples: newSamples,
      finishedLoopUnrolls: finishedLoopUnrolls,
      inProgressLoopUnrolls: inProgressLoopUnrolls
    )
  }

  private func executeObserveStmt(
    condition: Expr,
    command: ExecutionCommand
  ) -> ASTExecutionState {
    let observeResult = ExprEvaluator.evaluateExpr(condition, samples: samples)
    let newSamples = zip(samples, observeResult).compactMap { (sample, observeResult) -> ASTSample? in
      guard case .bool(let isSatisfied) = observeResult else {
        fatalError("Observe condition did not evaluate to Bool?")
      }
      if isSatisfied {
        return sample
      } else {
        return nil
      }
    }
    return ASTExecutionState(
      remainingStmts: Array(remainingStmts.dropFirst()),
      executionHistory: executionHistory + [command],
      samples: newSamples,
      finishedLoopUnrolls: finishedLoopUnrolls,
      inProgressLoopUnrolls: inProgressLoopUnrolls
    )
  }

  /// Execute a branching statement (e.g. an `if`-statement or a `while`-statement).
  /// Executes `ifBlock` if `condition` evalutes to `true` and executes `elseBlock` if `condition` evalutes to `false`.
  private func executeBranchingStmt(
    condition: Expr,
    ifBlock: [Stmt],
    elseBlock: [Stmt],
    command: ExecutionCommand,
    loopId: ASTNodeId? = nil
  ) -> ASTExecutionState {
    let conditionVarValues = ExprEvaluator.evaluateExpr(condition, samples: samples)
    let conditionValues = conditionVarValues.map { varValue -> Bool in
      guard case .bool(let boolValue) = varValue else {
        fatalError("Observe condition did not evaluate to Bool?")
      }
      return boolValue
    }
    return executeBranchingStmt(
      ifBlock: ifBlock,
      elseBlock: elseBlock,
      command: command,
      conditionValues: conditionValues,
      loopId: loopId
    )
  }
  
  /// Execute a branching statement, assuming that its condition evaluates to `conditionValues`.
  internal func executeBranchingStmt(
    ifBlock: [Stmt],
    elseBlock: [Stmt],
    command: ExecutionCommand,
    conditionValues: [Bool],
    loopId: ASTNodeId? = nil
  ) -> ASTExecutionState {
    let ifBlockSamples = zip(samples, conditionValues).filter { (sample, conditionValue) in
      return conditionValue == true
    }.map { $0.0 }
    let elseBlockSamples = zip(samples, conditionValues).filter { (sample, conditionValue) in
      return conditionValue == false
    }.map { $0.0 }

    // We know that we have an execution command because we checked it in executeFirstCommand
    switch command {
    case .stepOver:
      let afterIfBranchExecutionState = ASTExecutionState(
        remainingStmts: ifBlock,
        executionHistory: executionHistory,
        samples: ifBlockSamples,
        finishedLoopUnrolls: finishedLoopUnrolls,
        inProgressLoopUnrolls: inProgressLoopUnrolls.increasingUnrollCount(of: loopId)
      ).executeUntilEnd()
      let afterElseBranchExecutionState = ASTExecutionState(
        remainingStmts: elseBlock,
        executionHistory: executionHistory,
        samples: elseBlockSamples,
        finishedLoopUnrolls: finishedLoopUnrolls.commitUnrollCount(of: loopId, fromInProgressCount: inProgressLoopUnrolls),
        inProgressLoopUnrolls: inProgressLoopUnrolls.clearingUnrollCount(of: loopId)
      ).executeUntilEnd()

      return ASTExecutionState(
        remainingStmts: Array(remainingStmts.dropFirst()),
        executionHistory: executionHistory + [command],
        samples: afterIfBranchExecutionState.samples + afterElseBranchExecutionState.samples,
        finishedLoopUnrolls: .merging(afterIfBranchExecutionState.finishedLoopUnrolls, afterElseBranchExecutionState.finishedLoopUnrolls),
        inProgressLoopUnrolls: inProgressLoopUnrolls
      )
    case .stepInto(branch: true):
      return ASTExecutionState(
        remainingStmts: ifBlock + Array(remainingStmts.dropFirst()),
        executionHistory: executionHistory + [command],
        samples: ifBlockSamples,
        finishedLoopUnrolls: finishedLoopUnrolls,
        inProgressLoopUnrolls: inProgressLoopUnrolls.increasingUnrollCount(of: loopId)
      )
    case .stepInto(branch: false):
      return ASTExecutionState(
        remainingStmts: elseBlock + Array(remainingStmts.dropFirst()),
        executionHistory: executionHistory + [command],
        samples: elseBlockSamples,
        finishedLoopUnrolls: finishedLoopUnrolls.commitUnrollCount(of: loopId, fromInProgressCount: inProgressLoopUnrolls),
        inProgressLoopUnrolls: inProgressLoopUnrolls.clearingUnrollCount(of: loopId)
      )
    }
  }
}

extension ASTExecutionState {
  public var description: String {
    """
    ▽ ASTExecutionState
      ▷ location: \(remainingStmts.first?.range.lowerBound.description ?? "end")
      ▷ executionHistory: \(executionHistory.map(\.description))
      ▷ samples: \(samples.count)
    """
  }
}
