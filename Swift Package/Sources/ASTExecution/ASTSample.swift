import SimpleLanguageAST

/// A `SourceVariable` -> `VariableValue` mapping that describes the variable
/// values of a *single* sample during sampling-based execution of a program.
public struct ASTSample: Equatable {
  public let values: [SourceVariable: VariableValue]

  public init(values: [SourceVariable: VariableValue]) {
    self.values = values
  }

  /// Return a new sample in which `variable` takes the given `value`.
  func assigning(variable: SourceVariable, value: VariableValue) -> ASTSample {
    var newValues = values
    newValues[variable] = value
    return ASTSample(values: newValues)
  }
}
