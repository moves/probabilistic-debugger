/// The value a `SourceVariable` can take during execution. Used to model
/// variables of different types.
public enum VariableValue: Hashable, Comparable, CustomStringConvertible {
  case integer(Int)
  case bool(Bool)

  public var integerValue: Int? {
    switch self {
    case .integer(let value):
      return value
    default:
      return nil
    }
  }

  public var boolValue: Bool? {
    switch self {
    case .bool(let value):
      return value
    default:
      return nil
    }
  }

  public var description: String {
    switch self {
    case .integer(let value):
      return value.description
    case .bool(let value):
      return value.description
    }
  }

  public static func <(lhs: VariableValue, rhs: VariableValue) -> Bool {
    switch (lhs, rhs) {
    case (.integer(let lhsValue), .integer(let rhsValue)):
      return lhsValue < rhsValue
    case (.bool(let lhsValue), .bool(let rhsValue)):
      switch (lhsValue, rhsValue) {
      case (false, true):
        return true
      case (true, false):
        return false
      case (true, true), (false, false):
        return false
      }
    default:
      // Values are not comparable
      return false
    }
  }
}
