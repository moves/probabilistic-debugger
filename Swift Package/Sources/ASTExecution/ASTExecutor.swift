import SimpleLanguageAST

public enum ASTExecutor {
  public static func executeUntilEnd(
    stmts: [Stmt],
    numSamples: Int
  ) -> (samples: [ASTSample], loopUnrolls: LoopUnrolls) {
    let initialState = ASTExecutionState.initialState(for: stmts, numSamples: numSamples)
    let finalState = initialState.executeUntilEnd()
    return (finalState.samples, finalState.finishedLoopUnrolls)
  }

  public static func execute(
    stmts: [Stmt],
    commands: [ExecutionCommand],
    numSamples: Int
  ) -> (samples: [ASTSample], loopUnrolls: LoopUnrolls) {
    var currentState = ASTExecutionState.initialState(for: stmts, numSamples: numSamples)
    for command in commands {
      currentState = currentState.executeOneCommand(command)
    }
    return (currentState.samples, currentState.finishedLoopUnrolls)
  }
}
