import ASTExecution
import SimpleLanguageAST

extension ASTSample: ExpressibleByDictionaryLiteral {
  public init(dictionaryLiteral elements: (SourceVariable, VariableValue)...) {
    self.init(values: Dictionary(uniqueKeysWithValues: elements))
  }
}
