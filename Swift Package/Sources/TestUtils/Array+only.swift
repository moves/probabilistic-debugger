public extension Array {
  /// Asserts that the array consists of a single element and returns it.
  var only: Element {
    assert(self.count == 1)
    return self.first!
  }
}
