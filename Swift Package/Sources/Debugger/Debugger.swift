import ASTExecution
import ASTWPInference
import SimpleLanguageAST

fileprivate extension WPTerm {
  static func probability(of variable: SourceVariable, equalTo variableValue: VariableValue) -> WPTerm {
    switch variableValue {
    case .integer(let value):
      return .probability(of: variable, equalTo: WPTerm.integer(value))
    case .bool(let value):
      return .probability(of: variable, equalTo: WPTerm.bool(value))
    }
  }
}

public class Debugger {
  // MARK: - Private members
  
  private let program: [Stmt]
  
  /// The debugger can save execution states on a state stack.
  /// This is particularly useful to look into a branch using the 'step into' command and later returning to the state that does not have samples filtered out.
  /// The last element on the `stateStack` is always the current state.
  /// Must always contain at least one element.
  public var stateStack: [ASTExecutionState] {
    didSet {
      assert(!stateStack.isEmpty, "We should never remove the last execution state from the state stack")
    }
  }
  
  private var currentState: ASTExecutionState {
    get {
      return stateStack.last!
    }
    set {
      stateStack[0] = newValue
    }
  }
  
  private var annotatedExecutionHistory: AnnotatedExecutionHistory {
    return AnnotatedExecutionHistory(from: currentState.executionHistory, stmts: program)
  }
  
  
  // MARK: - Creating a debugger
  
  public init(program: [Stmt], sampleCount: Int) {
    self.program = program
    self.stateStack = [ASTExecutionState.initialState(for: program, numSamples: sampleCount)]
  }
  
  public init(_ other: Debugger) {
    self.program = other.program
    self.stateStack = other.stateStack
  }
  
  // MARK: - Retrieving current state
  
  public var sourceLocation: SourceLocation? {
    return sourceLocation(of: currentState)
  }
  
  public var samples: [ASTSample] {
    return currentState.samples
  }
  
  public var variableValuesRefinedUsingWP: [SourceVariable: [VariableValue: ClosedRange<Double>]] {
    var variableValues: [SourceVariable: [VariableValue: ClosedRange<Double>]] = [:]
    for variable in currentState.liveVariables {
      let possibleValues = Set(currentState.samples.map({ $0.values[variable]! }))
      var variableDistribution: [VariableValue: ClosedRange<Double>] = [:]
      for value in possibleValues {
        variableDistribution[value] = WPInferenceEngine.infer(
          term: .probability(of: variable, equalTo: value),
          for: annotatedExecutionHistory,
          loopBounds: currentState.finishedLoopUnrolls
        ).bounds
      }
      variableValues[variable] = variableDistribution
    }
    return variableValues
  }
  
  public func sourceLocation(of executionState: ASTExecutionState) -> SourceLocation? {
    return executionState.remainingStmts.first?.range.lowerBound
  }
  
  // MARK: - Step through the program
  
  /// Run the program until the end
  public func runUntilEnd() throws {
    currentState = currentState.executeUntilEnd()
  }
  
  /// Continue execution of the program to the next statement with debug info that is reachable by all execution branches.
  /// For normal instructions this means stepping to the next instruction that has debug info (and thus maps to a statement in the source program).
  /// For branch instruction, this means jumping to the immediate postdominator block.
  public func stepOver() throws {
    currentState = currentState.executeOneCommand(.stepOver)
  }
  
  /// If the program is currently at a `BranchInstruction`, either focus on the `true` or the `false` branch, discarding any samples that would not execute this branch.
  public func stepInto(branch: Bool) throws {
    currentState = currentState.executeOneCommand(.stepInto(branch: branch))
  }
  
  // MARK: - Saving and restoring states
  
  /// Save the current state on the state stack so it can be restored later using `restoreState`.
  public func saveState() {
    self.stateStack.append(currentState)
  }
  
  /// Restore the last saved state.
  public func restoreState() throws {
    if stateStack.count <= 1 {
      throw DebuggerError(message: "No state to restore on the states stack")
    }
    stateStack.removeLast()
  }
  
  /// Clears the stack of saved states and positions the debugger at this execution state
  public func jumpToState(_ state: ASTExecutionState) {
    self.stateStack = [state]
  }
}
