// swift-tools-version:5.2

import PackageDescription

let package = Package(
  name: "ProbabilisticDebugger",
  
  // MARK: - Products
  
  products: [
    .executable(name: "ppdb", targets: ["DebuggerConsole"]),
    .library(name: "libppdb", targets: [
      "ASTExecution",
      "ASTWPInference",
      "Debugger",
      "SimpleLanguageAST",
      "SimpleLanguageParser",
      "SimpleLanguageTypeChecker"
    ])
  ],
  
  dependencies: [
    .package(url: "https://github.com/apple/swift-argument-parser", from: "0.0.1"),
  ],
  
  // MARK: - Targets
  
  targets: [
    .target(
      name: "ASTExecution",
      dependencies: [
        "SimpleLanguageAST"
      ]
    ),
    .target(
      name: "ASTWPInference",
      dependencies: [
        "ASTExecution",
        "SimpleLanguageAST",
        "Utils"
      ]
    ),
    .target(
      name: "Debugger",
      dependencies: [
        "ASTExecution",
        "ASTWPInference",
        "SimpleLanguageAST"
      ]
    ),
    .target(
      name: "DebuggerConsole",
      dependencies: [
        .product(name: "ArgumentParser", package: "swift-argument-parser"),
        "ASTExecution",
        "Debugger",
        "SimpleLanguageAST",
        "SimpleLanguageParser",
        "SimpleLanguageTypeChecker",
        "Utils",
      ]
    ),
    .target(
      name: "SimpleLanguageAST",
      dependencies: [
        "Utils"
      ]
    ),
    .target(
      name: "SimpleLanguageParser",
      dependencies: [
        "SimpleLanguageAST"
    ]),
    .target(
      name: "SimpleLanguageTypeChecker",
      dependencies: [
        "SimpleLanguageAST",
        "SimpleLanguageParser",
    ]),
    .target(
      name: "TestUtils",
      dependencies: [
        "ASTExecution",
        "SimpleLanguageAST"
      ]
    ),
    .target(
      name: "Utils",
      dependencies: []
    ),
    
    // MARK: - Test targets

    .testTarget(
      name: "ASTExecutorTests",
      dependencies: [
        "ASTExecution",
        "SimpleLanguageAST",
        "SimpleLanguageParser",
        "SimpleLanguageTypeChecker",
        "TestUtils"
      ]
    ),
    .testTarget(
      name: "ASTWPInferenceTests",
      dependencies: [
        "ASTExecution",
        "ASTWPInference",
        "SimpleLanguageAST",
        "SimpleLanguageParser",
        "SimpleLanguageTypeChecker"
      ]
    ),
    .testTarget(
      name: "SimpleLanguageASTTests",
      dependencies: [
        "SimpleLanguageAST",
        "TestUtils",
      ]
    ),
    .testTarget(
      name: "SimpleLanguageParserTests",
      dependencies: [
        "SimpleLanguageAST",
        "SimpleLanguageParser",
        "TestUtils",
      ]
    ),
    .testTarget(
      name: "SimpleLanguageTypeCheckerTests",
      dependencies: [
        "SimpleLanguageAST",
        "SimpleLanguageParser",
        "SimpleLanguageTypeChecker",
        "TestUtils",
      ]
    ),
  ]
)
