import ASTExecution
import SimpleLanguageAST
import SimpleLanguageParser
import SimpleLanguageTypeChecker
import TestUtils

import XCTest

fileprivate func execute(_ sourceCode: String, numSamples: Int) -> [ASTSample] {
  let parser = Parser(sourceCode: sourceCode)
  let parsedAst = try! parser.parseFile()
  let ast = try! TypeCheckPipeline.typeCheck(stmts: parsedAst)
  let executionResult = ASTExecutor.executeUntilEnd(stmts: ast, numSamples: numSamples)
  return executionResult.samples
}

fileprivate func executeCountingLoopUnrolls(_ sourceCode: String, numSamples: Int) -> (samples: [ASTSample], loopUnrolls: LoopUnrolls) {
  let parser = Parser(sourceCode: sourceCode)
  let parsedAst = try! parser.parseFile()
  let ast = try! TypeCheckPipeline.typeCheck(stmts: parsedAst)
  let executionResult = ASTExecutor.executeUntilEnd(stmts: ast, numSamples: numSamples)
  return executionResult
}

class ASTExecutorRunUntilEndTests: XCTestCase {
  func testSingleAssignment() {
    let samples = execute("""
    int x = 42
    """, numSamples: 1)
    let x = SourceVariable(name: "x", disambiguationIndex: 1, type: .int)
    XCTAssertEqual(samples.only, [
      x: .integer(42)
    ])
  }

  func testExpressionEvaluationAssignment() {
    let samples = execute("""
    int x = 42
    int y = 12
    int z = x - y
    """, numSamples: 1)
    let x = SourceVariable(name: "x", disambiguationIndex: 1, type: .int)
    let y = SourceVariable(name: "y", disambiguationIndex: 1, type: .int)
    let z = SourceVariable(name: "z", disambiguationIndex: 1, type: .int)
    XCTAssertEqual(samples.only, [
      x: .integer(42),
      y: .integer(12),
      z: .integer(30),
    ])
  }

  func testDeterministicIfStmt() {
    let samples = execute("""
    int x = 42
    if true {
      x = x - 12
    }
    """, numSamples: 1)
    let x = SourceVariable(name: "x", disambiguationIndex: 1, type: .int)
    XCTAssertEqual(samples.only, [
      x: .integer(30),
    ])
  }

  func testProbabilisticIfLeadsToDeterministicResult() {
    let samples = execute("""
    int x = 0
    prob 0.5 {
      x = 1
    } else {
      x = 2
    }
    if x == 1 {
      x = x + 2
    } else {
      x = x + 1
    }
    """, numSamples: 1_000)
    let x = SourceVariable(name: "x", disambiguationIndex: 1, type: .int)
    let xValues = samples.map({
      $0.values[x]!.integerValue!
    })
    XCTAssertEqual(xValues.average, 3)
  }

  func testDeterministicLoopNeverIterated() {
    let (samples, loopUnrolls) = executeCountingLoopUnrolls("""
    int x = 0
    while 0 < x {
      x = x - 1
    }
    """, numSamples: 1)
    let x = SourceVariable(name: "x", disambiguationIndex: 1, type: .int)
    XCTAssertEqual(samples.only, [
      x: .integer(0),
    ])
    XCTAssertEqual(Array(loopUnrolls.values.values).only, 0)
  }

  func testDeterministicLoop() {
    let (samples, loopUnrolls) = executeCountingLoopUnrolls("""
    int x = 5
    while 0 < x {
      x = x - 1
    }
    x = x + 1
    """, numSamples: 1)
    let x = SourceVariable(name: "x", disambiguationIndex: 1, type: .int)
    XCTAssertEqual(samples.only, [
      x: .integer(1),
    ])
    XCTAssertEqual(Array(loopUnrolls.values.values).only, 5)
  }

  func testDeterministicNestedLoop() {
    let (samples, loopUnrolls) = executeCountingLoopUnrolls("""
    int x = 5
    while 0 < x {
      int y = x + 10
      while 0 < y {
        y = y - 1
      }

      x = x - 1
    }
    """, numSamples: 1)
    let x = SourceVariable(name: "x", disambiguationIndex: 1, type: .int)
    let y = SourceVariable(name: "y", disambiguationIndex: 1, type: .int)
    XCTAssertEqual(samples.only, [
      x: .integer(0),
      y: .integer(0)
    ])
    XCTAssertEqual(loopUnrolls.values.values.sorted(), [5, 15])
  }

  func testGeometricDistribution() {
    let (samples, loopUnrolls) = executeCountingLoopUnrolls("""
    int x = 0
    bool continue = true
    while continue {
      x = x + 1
      prob 0.5 {
        continue = false
      }
    }
    """, numSamples: 1_000)
    let x = SourceVariable(name: "x", disambiguationIndex: 1, type: .int)
    let xValues = samples.map({
      $0.values[x]!.integerValue!
    })
    XCTAssertEqual(xValues.average, 2, accuracy: 0.1)
    XCTAssertEqual(Array(loopUnrolls.values.values).only, 10, accuracy: 5)
  }
  
  func testProbStmt() {
    let samples = execute("""
    int x = 0
    prob 0.5 {
      x = 1
    } else {
      x = 2
    }
    """, numSamples: 1_000)
    let x = SourceVariable(name: "x", disambiguationIndex: 1, type: .int)
    let xValues = samples.map({
      $0.values[x]!.integerValue!
    })
    XCTAssertEqual(xValues.average, 1.5, accuracy: 0.1)
  }
}
