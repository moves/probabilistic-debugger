import ASTExecution
import SimpleLanguageAST
import SimpleLanguageParser
import SimpleLanguageTypeChecker

import XCTest

fileprivate func generateExecutionOutline(_ sourceCode: String, numSamples: Int) -> ExecutionOutline {
  let parser = Parser(sourceCode: sourceCode)
  let parsedAst = try! parser.parseFile()
  let ast = try! TypeCheckPipeline.typeCheck(stmts: parsedAst)
  return ExecutionOutlineGenerator.generateExecutionOutline(stmts: ast, numSamples: numSamples)
}

fileprivate extension ASTExecutionState {
  var line: Int? {
    return remainingStmts.first?.range.lowerBound.line
  }
}

fileprivate extension Array {
  func dropping(after index: Int) -> Array {
    if self.count > index {
      return Array(self[0..<index])
    } else {
      return self
    }
  }
}

// MARK: - Matching execution outlines against templates

fileprivate struct ExecutionOutlineTemplate: ExpressibleByArrayLiteral {
  typealias ArrayLiteralElement = ExecutionOutlineEntryTemplate
  
  let entries: [ExecutionOutlineEntryTemplate]
  
  init(_ entries: [ExecutionOutlineEntryTemplate]) {
    self.entries = entries
  }
  
  init(arrayLiteral elements: ExecutionOutlineEntryTemplate...) {
    self.entries = elements
  }
  
  func matches(_ outline: ExecutionOutline, loopBound: Int) -> Bool {
    let template = self
    if outline.entries.count != template.entries.count {
      return false
    }
    return zip(outline.entries, template.entries).allSatisfy { (entry, template) in
      template.matches(entry, loopBound: loopBound)
    }
  }
}

fileprivate enum ExecutionOutlineEntryTemplate {
  case statement(StateTemplate)
  case end(StateTemplate)
  case branch(StateTemplate, true: ExecutionOutlineTemplate?, false: ExecutionOutlineTemplate?)
  case loop(StateTemplate, iterations: [ExecutionOutlineTemplate], exitStates: [StateTemplate])
  
  func matches(_ entry: ExecutionOutlineEntry, loopBound: Int) -> Bool {
    switch (entry, self) {
    case (.statement(let state), .statement(let stateTemplate)):
      if !stateTemplate.matches(state: state) {
        return false
      }
      return true
    case (.end(let state), .end(let stateTemplate)):
      if !stateTemplate.matches(state: state) {
        return false
      }
      return true
    case (.branch(let state, let trueOutline, let falseOutline), .branch(let stateTemplate, let trueTemplate, let falseTemplate)):
    if !stateTemplate.matches(state: state) {
      return false
    }
    if !trueTemplate.matches(trueOutline, loopBound: loopBound) {
      return false
    }
    if !falseTemplate.matches(falseOutline, loopBound: loopBound) {
      return false
    }
    return true
    case (.loop(let state, let iterations, let exitStates), .loop(let stateTemplate, let iterationTemplates, let exitStateTemplates)):
      if !stateTemplate.matches(state: state) {
        return false
      }
      if !iterationTemplates.matches(iterations, loopBound: loopBound) {
        return false
      }
      if !exitStateTemplates.matches(states: exitStates, loopBound: loopBound) {
        return false
      }
      return true
    default:
      return false
    }
  }
}

extension Optional where Wrapped == ExecutionOutlineTemplate {
  func matches(_ outline: ExecutionOutline?, loopBound: Int) -> Bool {
    switch (outline, self) {
    case (.some(let outline), .some(let template)):
      return template.matches(outline, loopBound: loopBound)
    case (nil, nil):
      return true
    default:
      return false
    }
  }
}

extension Array where Element == ExecutionOutlineTemplate {
  func matches(_ outlines: [ExecutionOutline], loopBound: Int) -> Bool {
    let selfBounded = self.dropping(after: loopBound)
    let outlinesBounded = outlines.dropping(after: loopBound)
    if selfBounded.count != outlinesBounded.count {
      return false
    }
    return zip(selfBounded, outlinesBounded).allSatisfy { (template, outline) in
      return template.matches(outline, loopBound: loopBound)
    }
  }
}

fileprivate enum StateTemplate {
  case approximateCount(count: Int, error: Double)
  case exact([ASTSample])
  
  static func exact(_ singleSamle: ASTSample) -> StateTemplate {
    return .exact([singleSamle])
  }
  
  func matches(state: ASTExecutionState) -> Bool {
    switch self {
    case .approximateCount(let count, let error):
      if Double(count) * (1 - error) <= Double(state.samples.count) &&
          Double(state.samples.count) <= Double(count) * (1 + error) {
        return true
      } else {
        return false
      }
    case .exact(let samples):
      return state.samples == samples
    }
  }
}

extension Array where Element == StateTemplate {
  func matches(states: [ASTExecutionState], loopBound: Int) -> Bool {
    let selfBounded = self.dropping(after: loopBound)
    let statesBounded = states.dropping(after: loopBound)
    
    if selfBounded.count != statesBounded.count {
      return false
    }
    return zip(selfBounded, statesBounded).allSatisfy({ $0.0.matches(state: $0.1 )})
  }
}

// MARK: - Tests

class ExecutionOutlineGeneratorTests: XCTestCase {
  func testStraightLineDeterministicExecution() {
    let source = """
    int x = 10
    int y = 5
    y = y + 6
    int z = x + y
    """
    let outline = generateExecutionOutline(source, numSamples: 1)
    let x = SourceVariable(name: "x", disambiguationIndex: 1, type: .int)
    let y = SourceVariable(name: "y", disambiguationIndex: 1, type: .int)
    let z = SourceVariable(name: "z", disambiguationIndex: 1, type: .int)

    XCTAssertEqual(outline.entries.count, 5)
    
    XCTAssertEqual(outline.entries[0].state.samples.only, [:])
    XCTAssertEqual(outline.entries[0].state.executionHistory, [])
    XCTAssertEqual(outline.entries[0].state.line, 1)
    
    XCTAssertEqual(outline.entries[1].state.samples.only, [
      x: .integer(10),
    ])
    XCTAssertEqual(outline.entries[1].state.executionHistory, [.stepOver])
    XCTAssertEqual(outline.entries[1].state.line, 2)
    
    XCTAssertEqual(outline.entries[2].state.samples.only, [
      x: .integer(10),
      y: .integer(5),
    ])
    XCTAssertEqual(outline.entries[2].state.executionHistory, [.stepOver, .stepOver])
    XCTAssertEqual(outline.entries[2].state.line, 3)
    
    XCTAssertEqual(outline.entries[3].state.samples.only, [
      x: .integer(10),
      y: .integer(11),
    ])
    XCTAssertEqual(outline.entries[3].state.executionHistory, [.stepOver, .stepOver, .stepOver])
    XCTAssertEqual(outline.entries[3].state.line, 4)
    
    XCTAssertEqual(outline.entries[4].state.samples.only, [
      x: .integer(10),
      y: .integer(11),
      z: .integer(21)
    ])
    XCTAssertEqual(outline.entries[4].state.executionHistory, [.stepOver, .stepOver, .stepOver, .stepOver])
    XCTAssertEqual(outline.entries[4].state.line, nil)
    
    let executionOutlineTemplate: ExecutionOutlineTemplate = [
      .statement(.exact([:])),
      .statement(.exact([x:.integer(10)])),
      .statement(.exact([x:.integer(10), y: .integer(5)])),
      .statement(.exact([x:.integer(10), y: .integer(11)])),
      .end(.exact([x:.integer(10), y: .integer(11), z: .integer(21)])),
    ]
    XCTAssert(executionOutlineTemplate.matches(outline, loopBound: 10))
  }
  
  func testDeterministicIfStatementWithBranchExecuted() {
    let source = """
    int x = 1
    if x == 1 {
      x = x + 2
    }
    """
    let outline = generateExecutionOutline(source, numSamples: 1)
    let x = SourceVariable(name: "x", disambiguationIndex: 1, type: .int)
    
    let executionOutlineTemplate: ExecutionOutlineTemplate = [
      .statement(.exact([:])),
      .branch(.exact([x: .integer(1)]), true: [
        .statement(.exact([x: .integer(1)])),
        .end(.exact([x: .integer(3)]))
      ], false: nil),
      .end(.exact([x: .integer(3)]))
    ]
    
    XCTAssert(executionOutlineTemplate.matches(outline, loopBound: 10))
  }
  
  func testDeterministicIfStatementWithBranchNotExecuted() {
    let source = """
    int x = 1
    if x == 2 {
      x = x + 2
    }
    """
    let outline = generateExecutionOutline(source, numSamples: 1)
    let x = SourceVariable(name: "x", disambiguationIndex: 1, type: .int)
    
    let executionOutlineTemplate: ExecutionOutlineTemplate = [
      .statement(.exact([:])),
      .branch(.exact([x: .integer(1)]), true: [
        .statement(.exact([])),
        .end(.exact([]))
      ], false: nil),
      .end(.exact([x: .integer(1)]))
    ]
    XCTAssert(executionOutlineTemplate.matches(outline, loopBound: 10))
  }
  
  func testObserveFiltersOutAllSamples() {
    let source = """
    int x = 1
    observe(x == 2)
    x = x + 2
    """
    let outline = generateExecutionOutline(source, numSamples: 1)
    let x = SourceVariable(name: "x", disambiguationIndex: 1, type: .int)
    
    let executionOutlineTemplate: ExecutionOutlineTemplate = [
      .statement(.exact([:])),
      .statement(.exact([x: .integer(1)])),
      .statement(.exact([])),
      .end(.exact([]))
    ]
    XCTAssert(executionOutlineTemplate.matches(outline, loopBound: 10))
  }
  
  func testDeterministicLoop() {
    let source = """
    int x = 3
    while 0 < x {
      x = x - 1
    }
    """
    let outline = generateExecutionOutline(source, numSamples: 1)
    let x = SourceVariable(name: "x", disambiguationIndex: 1, type: .int)

    let executionOutlineTemplate: ExecutionOutlineTemplate = [
      .statement(.exact([:])),
      .loop(.exact([x: .integer(3)]), iterations: [
        [
          .statement(.exact([x: .integer(3)])),
          .end(.exact([x: .integer(2)])),
        ],
        [
          .statement(.exact([x: .integer(2)])),
          .end(.exact([x: .integer(1)])),
        ],
        [
          .statement(.exact([x: .integer(1)])),
          .end(.exact([x: .integer(0)])),
        ],
      ], exitStates: [
        .exact([]),
        .exact([]),
        .exact([]),
        .exact([x: .integer(0)]),
      ]),
      .end(.exact([x: .integer(0)]))
    ]
    
    XCTAssert(executionOutlineTemplate.matches(outline, loopBound: 10))
  }
  
  func testGeometricDistributionWithProbStmt() {
    let source = """
    bool continue = true
    int x = 0
    while continue {
      x = x + 1
      prob 0.5 {
        continue = false
      }
    }
    """
    let outline = generateExecutionOutline(source, numSamples: 100)
    XCTAssertEqual(outline.entries.count, 4)
    guard case .loop(state: _, iterations: let iterations, exitStates: _) = outline.entries[2] else {
      XCTFail("Third oultine entry should be a loop")
      return
    }
    XCTAssertEqual(iterations[0].entries.count, 3)
    guard case .end(state: let firstIterationFinishedState) = iterations[0].entries[2] else {
      XCTFail("Should be an end state")
      return
    }
    XCTAssertEqual(firstIterationFinishedState.samples.count, 100)
  }
}
