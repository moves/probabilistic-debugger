import ASTExecution
import SimpleLanguageAST
import SimpleLanguageParser
import SimpleLanguageTypeChecker
import TestUtils

import XCTest

fileprivate extension Array {
  var only: Element {
    assert(self.count == 1)
    return self.first!
  }
}

fileprivate func execute(_ sourceCode: String, commands: [ExecutionCommand], numSamples: Int) -> [ASTSample] {
  let parser = Parser(sourceCode: sourceCode)
  let parsedAst = try! parser.parseFile()
  let ast = try! TypeCheckPipeline.typeCheck(stmts: parsedAst)
  let executionResult = ASTExecutor.execute(stmts: ast, commands: commands, numSamples: numSamples)
  return executionResult.samples
}
//
//fileprivate func executeCountingLoopUnrolls(_ sourceCode: String, numSamples: Int) -> (samples: [ASTSample], loopUnrolls: LoopUnrolls) {
//  let parser = Parser(sourceCode: sourceCode)
//  let parsedAst = try! parser.parseFile()
//  let ast = try! TypeCheckPipeline.typeCheck(stmts: parsedAst)
//  let executionResult = ASTExecutor.executeUntilEnd(stmts: ast, numSamples: numSamples)
//  return executionResult
//}

class ASTExecutorStepwiseTests: XCTestCase {
  func testStepIntoIf() {
    let source = """
    int x = 0
    prob 0.5 {
      x = 1
    } else {
      x = 2
    }
    if x == 1 {
      x = x + 2
    } else {
      x = x + 1
    }
    """
    let x = SourceVariable(name: "x", disambiguationIndex: 1, type: .int)

    func executeAndRetrieveAverageXValue(commands: [ExecutionCommand]) -> Double {
      let samples = execute(source, commands: commands, numSamples: 1_000)
      let xValues = samples.map({
        $0.values[x]!.integerValue!
      })
      return xValues.average
    }
    XCTAssertEqual(executeAndRetrieveAverageXValue(commands: [.stepOver, .stepOver]), 1.5, accuracy: 0.1)
    XCTAssertEqual(executeAndRetrieveAverageXValue(commands: [.stepOver, .stepOver, .stepInto(branch: true)]), 1)
    XCTAssertEqual(executeAndRetrieveAverageXValue(commands: [.stepOver, .stepOver, .stepInto(branch: false)]), 2)
    XCTAssertEqual(executeAndRetrieveAverageXValue(commands: [.stepOver, .stepOver, .stepInto(branch: true), .stepOver]), 3)
  }

  func testStepThroughGeometricDistribution() {
    let source = """
    int x = 0
    bool continue = true
    while continue {
      x = x + 1
      prob 0.5 {
        continue = false
      }
    }
    """
    let x = SourceVariable(name: "x", disambiguationIndex: 1, type: .int)

    func executeAndRetrieveAverageXValue(commands: [ExecutionCommand]) -> Double {
      let samples = execute(source, commands: commands, numSamples: 1_000)
      let xValues = samples.map({
        $0.values[x]!.integerValue!
      })
      return xValues.average
    }
    XCTAssertEqual(executeAndRetrieveAverageXValue(commands: [.stepOver]), 0)
    XCTAssertEqual(executeAndRetrieveAverageXValue(commands: [.stepOver, .stepOver]), 0)
    XCTAssertEqual(executeAndRetrieveAverageXValue(commands: [.stepOver, .stepOver, .stepInto(branch: true)]), 0)
    XCTAssertEqual(executeAndRetrieveAverageXValue(commands: [.stepOver, .stepOver, .stepInto(branch: true), .stepOver]), 1)
    XCTAssertEqual(executeAndRetrieveAverageXValue(commands: [.stepOver, .stepOver, .stepInto(branch: true), .stepOver, .stepInto(branch: false), .stepInto(branch: true), .stepOver]), 2)
    XCTAssertEqual(executeAndRetrieveAverageXValue(commands: [.stepOver, .stepOver, .stepInto(branch: true), .stepOver, .stepInto(branch: false), .stepInto(branch: true), .stepOver, .stepInto(branch: false)]), 2)
  }
}
