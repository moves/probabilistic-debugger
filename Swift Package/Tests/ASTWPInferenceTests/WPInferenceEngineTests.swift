import ASTExecution
import ASTWPInference
import SimpleLanguageAST
import SimpleLanguageParser
import SimpleLanguageTypeChecker

import XCTest

fileprivate func compile(source: String) -> [Stmt] {
  let parser = Parser(sourceCode: source)
  let parsedAst = try! parser.parseFile()
  return try! TypeCheckPipeline.typeCheck(stmts: parsedAst)
}

fileprivate func inferBounds(of variable: SourceVariable, beingEqualTo expectedValue: Int, commands: [ExecutionCommand], stmts: [Stmt], loopBounds: LoopUnrolls = .empty) -> ClosedRange<Double> {
  return infer(variable: variable, beingEqualTo: expectedValue, commands: commands, stmts: stmts, loopBounds: loopBounds).bounds
}

fileprivate func infer(variable: SourceVariable, beingEqualTo expectedValue: Int, commands: [ExecutionCommand], stmts: [Stmt], loopBounds: LoopUnrolls = .empty) -> WPInferenceTerm {
  let executionHistory = AnnotatedExecutionHistory(from: commands, stmts: stmts)
  let term = WPTerm.boolToInt(.equal(lhs: .variable(variable), rhs: .integer(expectedValue)))
  let result = WPInferenceEngine.infer(term: term, for: executionHistory, loopBounds: loopBounds)
  return result
}

class WPInferenceEngineTests: XCTestCase {
  func testSingleInstructionProgram() {
    let source = """
    int x = 5
    """
    let stmts = compile(source: source)
    
    let x = SourceVariable(name: "x", disambiguationIndex: 1, type: .int)
    
    XCTAssertEqual(inferBounds(of: x, beingEqualTo: 5, commands: [.stepOver], stmts: stmts), 1...1)
    XCTAssertEqual(inferBounds(of: x, beingEqualTo: 6, commands: [.stepOver], stmts: stmts), 0...0)
  }
  
  func testMultipleStraightLineInstruction() {
    let source = """
    int x = 5
    int y = 3
    y = y + 2
    int z = x + y
    """
    let stmts = compile(source: source)
    
    let y = SourceVariable(name: "y", disambiguationIndex: 1, type: .int)
    let z = SourceVariable(name: "z", disambiguationIndex: 1, type: .int)
    
    XCTAssertEqual(inferBounds(of: y, beingEqualTo: 3, commands: [.stepOver, .stepOver], stmts: stmts), 1...1)
    XCTAssertEqual(inferBounds(of: y, beingEqualTo: 5, commands: [.stepOver, .stepOver], stmts: stmts), 0...0)
    XCTAssertEqual(inferBounds(of: y, beingEqualTo: 3, commands: [.stepOver, .stepOver, .stepOver], stmts: stmts), 0...0)
    XCTAssertEqual(inferBounds(of: y, beingEqualTo: 5, commands: [.stepOver, .stepOver, .stepOver], stmts: stmts), 1...1)
    
    XCTAssertEqual(inferBounds(of: z, beingEqualTo: 10, commands: [.stepOver, .stepOver, .stepOver, .stepOver], stmts: stmts), 1...1)
    XCTAssertEqual(inferBounds(of: z, beingEqualTo: 9, commands: [.stepOver, .stepOver, .stepOver, .stepOver], stmts: stmts), 0...0)
  }
  
  func testProbablisticProgramWithIf() {
    let source = """
    int x = 1
    prob 0.3 {
      x = 2
    }
    """
    let stmts = compile(source: source)
    
    let x = SourceVariable(name: "x", disambiguationIndex: 1, type: .int)
    
    XCTAssertEqual(inferBounds(of: x, beingEqualTo: 2, commands: [.stepOver, .stepOver], stmts: stmts), 0.3...0.3)
    XCTAssertEqual(inferBounds(of: x, beingEqualTo: 1, commands: [.stepOver, .stepOver], stmts: stmts), 0.7...0.7)
    XCTAssertEqual(inferBounds(of: x, beingEqualTo: 2, commands: [.stepOver, .stepInto(branch: true), .stepOver], stmts: stmts), 1...1)
    XCTAssertEqual(inferBounds(of: x, beingEqualTo: 1, commands: [.stepOver, .stepInto(branch: true)], stmts: stmts), 1...1)
    XCTAssertEqual(inferBounds(of: x, beingEqualTo: 1, commands: [.stepOver, .stepInto(branch: false)], stmts: stmts), 1...1)
  }
  
  func testGeometricDistribution() {
    let source = """
    bool continue = true
    int x = 0
    while continue {
      x = x + 1
      prob 0.5 {
        continue = false
      }
    }
    """
    let stmts = compile(source: source)
    
    let (_, loopUnrolls) = ASTExecutor.executeUntilEnd(stmts: stmts, numSamples: 1_000)
    
    let x = SourceVariable(name: "x", disambiguationIndex: 1, type: .int)
    
    XCTAssert(inferBounds(of: x, beingEqualTo: 1, commands: [.stepOver, .stepOver, .stepOver], stmts: stmts, loopBounds: loopUnrolls).contains(0.5))
    XCTAssert(inferBounds(of: x, beingEqualTo: 2, commands: [.stepOver, .stepOver, .stepOver], stmts: stmts, loopBounds: loopUnrolls).contains(0.25))
    XCTAssertEqual(inferBounds(of: x, beingEqualTo: 1, commands: [
      .stepOver,
      .stepOver,
      .stepInto(branch: true),
      .stepOver
    ], stmts: stmts, loopBounds: loopUnrolls), 1...1)
    let twoLoopIterations = infer(variable: x, beingEqualTo: 1, commands: [
      .stepOver,
      .stepOver,
      .stepInto(branch: true),
      .stepOver,
      .stepInto(branch: false),
      .stepInto(branch: true)
    ], stmts: stmts, loopBounds: loopUnrolls)
    XCTAssertEqual(twoLoopIterations.bounds, 1...1)
    XCTAssertEqual(twoLoopIterations.wpOne.doubleValue, 0.5)
  }
}
